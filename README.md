# CarShop

CarShop is a microservice exposing endpoints to manage cars' purchases.

Service uses REST API and can be easily integrated with any front-end application.


## Run instruction

In order to run microservice, MySQL database has to be turned on, using 8280 port. 

To download additional data from external microservice, Mockoon project has to be turned on.

List of endpoints is present in Postman collection in respository.

Started Keycloak on the port 8080 in order to authenticate with JWT token.


## Test instruction

In order to build project with Gradle, Mockoon project has to be turned on. Mockoon environment can be found in the repository.

MySQL can be turned off, tests work with H2 internal database.
