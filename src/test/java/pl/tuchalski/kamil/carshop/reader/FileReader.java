package pl.tuchalski.kamil.carshop.reader;

import org.apache.commons.io.FileUtils;
import org.springframework.util.ResourceUtils;

import java.io.IOException;
import java.nio.charset.StandardCharsets;

/**
 * Class reading from file.
 */
public class FileReader {

    /**
     * Reads from file
     *
     * @param path path to the file
     * @return the file as string
     * @throws IOException if there is a problem reading the file
     */
    public static String readFile(final String path) throws IOException {
        return FileUtils.readFileToString(
                ResourceUtils.getFile("src/test/resources/" + path),
                StandardCharsets.UTF_8
        );
    }
}
