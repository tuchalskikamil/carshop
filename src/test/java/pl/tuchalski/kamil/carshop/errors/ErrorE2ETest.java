package pl.tuchalski.kamil.carshop.errors;

import org.json.JSONException;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.skyscreamer.jsonassert.JSONAssert;
import org.skyscreamer.jsonassert.JSONCompareMode;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.*;
import org.springframework.test.context.ActiveProfiles;
import pl.tuchalski.kamil.carshop.client.Client;
import pl.tuchalski.kamil.carshop.client.ClientRepository;

import java.io.IOException;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;
import static pl.tuchalski.kamil.carshop.reader.FileReader.readFile;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@ActiveProfiles("test")
public class ErrorE2ETest {
    @Autowired
    private TestRestTemplate restTemplate;

    @MockBean
    private ClientRepository clientRepository;

    @LocalServerPort
    private int servicePort;

    @Test
    @DisplayName("Should return  internal server error")
    void shouldReturnInternalServerError() throws IOException, JSONException {
        //given
        when(clientRepository.save(any(Client.class))).thenThrow(new RuntimeException("Unexpected"));
        final HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        final HttpEntity<String> request = new HttpEntity<>(readFile("client/client.json"), headers);
        //when
        ResponseEntity<String> result = restTemplate.postForEntity(
                "http://localhost:" + servicePort + "/clients",
                request,
                String.class
        );
        //then
        assertThat(result.getStatusCode()).isEqualTo(HttpStatus.INTERNAL_SERVER_ERROR);
        JSONAssert.assertEquals(
                readFile("error/serverError.json"),
                result.getBody(),
                JSONCompareMode.NON_EXTENSIBLE
        );

    }

}