package pl.tuchalski.kamil.carshop.car;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class CarFilterServiceTest {

    private CarFilterService carFilterService;

    @Mock
    private CarRepository carRepository;

    @Mock
    private CarToCarDtoMapper carToCarDtoMapper;

    @BeforeEach
    void setup() {
        carFilterService = new CarFilterService(carRepository, carToCarDtoMapper);
    }

    @Test
    @DisplayName("Should return cars filtered by brand")
    void shouldReturnFilteredByBrand() {
        //given
        final CarDto carDto1 = new CarDtoBuilder().build();
        final CarDto carDto2 = new CarDtoBuilder().
                withBrand("Ferrari").
                withModel("318").
                withProductionYear(1980).
                withSold(true).build();
        final List<CarDto> carDtos = List.of(carDto1, carDto2);
        final List<Car> cars = List.of(new Car());
        when(carRepository.findByBrand("Volkswagen")).thenReturn(cars);
        when(carToCarDtoMapper.mapCars(cars)).thenReturn(carDtos);
        //when
        final List<CarDto> result = carFilterService.getCarsFilteredByBrand("Volkswagen");
        //then
        assertThat(result).containsExactlyInAnyOrderElementsOf(carDtos);
    }

    @Test
    @DisplayName("Should return cars filtered by sold")
    void shouldReturnFilteredBySold() {
        //given
        final CarDto carDto1 = new CarDtoBuilder().build();
        final List<CarDto> carDtos = List.of(carDto1);
        final List<Car> cars = List.of(new Car());
        when(carRepository.findBySold(true)).thenReturn(cars);
        when(carToCarDtoMapper.mapCars(cars)).thenReturn(carDtos);
        //when
        final List<CarDto> result = carFilterService.getCarsFilteredBySold(true);
        //then
        assertThat(result).containsExactlyInAnyOrderElementsOf(carDtos);
    }

    @Test
    @DisplayName("Should return cars filtered by productions year")
    void shouldReturnFilteredByProductionYear() {
        //given
        final CarDto carDto1 = new CarDtoBuilder().build();
        final List<CarDto> carDtos = List.of(carDto1);
        final List<Car> cars = List.of(new Car());
        when(carRepository.findByProductionYearBetween(2014, 2020)).thenReturn(cars);
        when(carToCarDtoMapper.mapCars(cars)).thenReturn(carDtos);
        //when
        final List<CarDto> result = carFilterService.getCarsFilteredByProductionYear(2014, 2020);
        //then
        assertThat(result).containsExactlyInAnyOrderElementsOf(carDtos);
    }

    @Test
    @DisplayName("Should return cars filtered by price")
    void shouldReturnFilteredByPrice(){
        //given
        final CarDto carDto = new CarDtoBuilder().build();
        final List<CarDto> carDtos = List.of(carDto);
        final List<Car> cars = List.of(new Car());
        when(carRepository.findByPriceBetween(3000, 150000)).thenReturn(cars);
        when(carToCarDtoMapper.mapCars(cars)).thenReturn(carDtos);
        //when
        final List<CarDto> result = carFilterService.getCarsFilteredByPrice(3000, 150000);
        //then
        assertThat(result).containsExactlyInAnyOrderElementsOf(carDtos);
    }
}
