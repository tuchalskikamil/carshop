package pl.tuchalski.kamil.carshop.car;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.modelmapper.ModelMapper;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class CarServiceTest {

    private CarService carService;

    @Mock
    private CarRepository carRepository;

    @Mock
    private ModelMapper modelMapper;

    @Mock
    private CarToCarDtoMapper carToCarDtoMapper;

    @BeforeEach
    void setup() {
        carService = new CarService(carRepository, modelMapper, carToCarDtoMapper);
    }

    @Test
    @DisplayName("Should return list of Cars")
    void shouldReturnListOfCars() {
        //given
        final CarDto carDto1 = new CarDtoBuilder().build();
        final CarDto carDto2 = new CarDtoBuilder().
                withBrand("Ferrari").
                withModel("318").
                withProductionYear(1980).
                withSold(true).build();
        final List<CarDto> carDtos = List.of(carDto1, carDto2);
        final List<Car> cars = List.of(new Car());
        when(carRepository.findAll()).thenReturn(cars);
        when(carToCarDtoMapper.mapCars(cars)).thenReturn(carDtos);
        //when
        final List<CarDto> result = carService.getAllCars();
        //then
        assertThat(result).containsExactlyInAnyOrderElementsOf(carDtos);
    }

    @Test
    @DisplayName("Should verify save")
    void shouldVerifySave() {
        //given
        final Car car = new Car();
        final CarDto carDto = new CarDtoBuilder().build();
        when(modelMapper.map(carDto, Car.class)).thenReturn(car);
        //when
        carService.addCar(carDto);
        //then
        verify(carRepository).save(car);
    }
}
