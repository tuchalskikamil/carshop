package pl.tuchalski.kamil.carshop.car;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class CarControllerTest {
    @Mock
    private CarService carService;
    private CarController carController;

    @BeforeEach
    void setup() {
        carController = new CarController(carService);
    }

    @Test
    @DisplayName("Should return list of Cars")
    void shouldReturnListOfCars() {
        //given
        final List<CarDto> cars = List.of(
                new CarDtoBuilder().build(),
                new CarDtoBuilder().withSold(true).build());
        when(carService.getAllCars()).thenReturn(cars);
        //when
        final List<CarDto> result = carController.getAllCars();
        //then
        assertThat(result).containsExactlyInAnyOrderElementsOf(cars);
    }

    @Test
    @DisplayName("Should return status 204-NO CONTENT")
    void shouldReturnStatusOk() {
        //given
        final CarDto carDto = new CarDtoBuilder().build();
        //when
        final ResponseEntity<String> result = carController.addCar(carDto);
        //then
        assertThat(result.getStatusCode()).isEqualTo(HttpStatus.NO_CONTENT);
    }

    @Test
    @DisplayName("Should return empty body")
    void shouldReturnEmptyBody() {
        //given
        final CarDto carDto = new CarDtoBuilder().build();
        //when
        final ResponseEntity<String> result = carController.addCar(carDto);
        //then
        assertThat(result.getBody()).isNull();
    }
}