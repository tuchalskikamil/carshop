package pl.tuchalski.kamil.carshop.car;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.modelmapper.spi.MappingContext;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class CarIdToCarConverterTest {

    private final CarIdToCarConverter carIdToCarConverter = new CarIdToCarConverter();

    @Mock
    private MappingContext<Long, Car> context;

    @Test
    @DisplayName("Should return convert car")
    void shouldReturnConvertCar() {
        //given
        final Car car = new Car();
        car.setId(1L);
        when(context.getSource()).thenReturn(1L);
        //when
        final Car result = carIdToCarConverter.convert(context);
        //then
        assertThat(result).isEqualTo(car);
    }
}