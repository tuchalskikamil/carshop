package pl.tuchalski.kamil.carshop.car;

import java.time.Instant;


public class CarDtoBuilder {
    private String brand;
    private String model;
    private int productionYear;
    private int price;
    private Instant timestamp;
    private Long id;
    private Boolean sold;
    private long daysInCarsShop;
    private String colour;
    private String numberOfDoors;
    private String bodyType;

    public CarDtoBuilder() {
        this.brand = "Volkswagen";
        this.model = "Passat";
        this.productionYear = 2015;
        this.price = 85000;
        this.timestamp = Instant.parse("2022-01-05T23:28:56Z");
        this.id = 1L;
        this.sold = true;
        this.daysInCarsShop = 15;
        this.colour = "black";
        this.numberOfDoors = "4/5";
        this.bodyType = "sport";
    }

    public CarDto build() {
        return new CarDto(this.brand,
                this.model,
                this.productionYear,
                this.price,
                this.timestamp,
                this.id,
                this.sold,
                this.daysInCarsShop,
                this.colour,
                this.numberOfDoors,
                this.bodyType);
    }

    public CarDtoBuilder withDaysInCarsShop(final long daysInCarsShop) {
        this.daysInCarsShop = daysInCarsShop;
        return this;
    }

    public CarDtoBuilder withSold(final Boolean sold) {
        this.sold = sold;
        return this;
    }

    public CarDtoBuilder withId(final Long id) {
        this.id = id;
        return this;
    }

    public CarDtoBuilder withTimestamp(final Instant timestamp) {
        this.timestamp = timestamp;
        return this;
    }

    public CarDtoBuilder withBrand(final String brand) {
        this.brand = brand;
        return this;
    }

    public CarDtoBuilder withModel(final String model) {
        this.model = model;
        return this;
    }

    public CarDtoBuilder withProductionYear(final int productionYear) {
        this.productionYear = productionYear;
        return this;
    }

    public CarDtoBuilder withPrice(final int price) {
        this.price = price;
        return this;
    }

    public CarDtoBuilder withColour(final String colour) {
        this.colour=colour;
       return this;
    }
    public CarDtoBuilder withNumberOfDoors(final String numberOfDoors) {
        this.numberOfDoors = numberOfDoors;
        return this;
    }
    public CarDtoBuilder withBodyType(final String bodyType) {
        this.bodyType = bodyType;
        return this;
    }
}