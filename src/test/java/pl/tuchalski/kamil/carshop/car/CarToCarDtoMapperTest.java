package pl.tuchalski.kamil.carshop.car;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.modelmapper.ModelMapper;
import pl.tuchalski.kamil.carshop.car.audi.AudiInformationService;
import pl.tuchalski.kamil.carshop.car.audi.CarAudi;
import pl.tuchalski.kamil.carshop.carPurchase.CarPurchase;
import pl.tuchalski.kamil.carshop.carPurchase.CarPurchaseRepository;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class CarToCarDtoMapperTest {

    private CarToCarDtoMapper carToCarDtoMapper;

    @Mock
    private CalculatorDaysInCarShop calculatorDaysInCarShop;

    @Mock
    private CarPurchaseRepository carPurchaseRepository;

    @Mock
    private AudiInformationService audiInformationService;

    @Mock
    private ModelMapper modelMapper;

    @BeforeEach
    void setup() {
        carToCarDtoMapper = new CarToCarDtoMapper(modelMapper,
                calculatorDaysInCarShop,
                carPurchaseRepository,
                audiInformationService);
    }


    @Test
    @DisplayName("Should return list of Cars")
    void shouldReturnListOfCars() {
        //given
        final CarDto carDto1In = new CarDtoBuilder().withDaysInCarsShop(0).build();
        final CarDto carDto2In = new CarDtoBuilder().withBrand("Audi").withDaysInCarsShop(0).withModel("a1").build();
        final CarDto carDto1Out = new CarDtoBuilder().withDaysInCarsShop(5).build();
        final CarDto carDto2Out = new CarDtoBuilder().withBrand("Audi").withDaysInCarsShop(4).withModel("a1").build();

        final List<CarDto> carDtosOut = List.of(carDto1Out, carDto2Out);
        final Car car1 = new CarBuilder().build();
        final Car car2 = new CarBuilder().witBrand("Audi").witModel("a1").build();
        final List<Car> cars = List.of(car1, car2);
        final CarPurchase carPurchase = new CarPurchase();
        when(calculatorDaysInCarShop.daysInCarShopToDate(eq(carDto1In), any())).thenReturn(5L);
        when(calculatorDaysInCarShop.daysInCarShopToDate(eq(carDto2In), any())).thenReturn(4L);
        when(carPurchaseRepository.findByCarId(1L)).thenReturn(carPurchase);
        when(modelMapper.map(car1, CarDto.class)).thenReturn(carDto1In);
        when(modelMapper.map(car2, CarDto.class)).thenReturn(carDto2In);
        when(audiInformationService.fetchAudiInformation(car2.getModel())).thenReturn(new CarAudi("Audi", "a1",
                "black", "4/5", "sport", 1L));

        //when
        final List<CarDto> result = carToCarDtoMapper.mapCars(cars);
        //then
        assertThat(result).containsExactlyInAnyOrderElementsOf(carDtosOut);
    }
}

