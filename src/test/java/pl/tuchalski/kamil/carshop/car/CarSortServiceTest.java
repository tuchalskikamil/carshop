package pl.tuchalski.kamil.carshop.car;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class CarSortServiceTest {

    private CarSortService carSortService;

    @Mock
    private CarRepository carRepository;
    @Mock
    private CarToCarDtoMapper carToCarDtoMapper;

    @BeforeEach
    void setup() {
        carSortService = new CarSortService(carRepository, carToCarDtoMapper);
    }

    @Test
    @DisplayName("Should return cars sorted by price")
    void shouldReturnCars() {
        // given
        final CarDto carDto1 = new CarDtoBuilder().build();
        final CarDto carDto2 = new CarDtoBuilder().withPrice(1500).build();
        final List<CarDto> carDtos = List.of(carDto1, carDto2);
        final List<Car> cars = List.of(new Car());
        when(carRepository.findByOrderByPrice()).thenReturn(cars);
        when(carToCarDtoMapper.mapCars(cars)).thenReturn(carDtos);
        //when
        final List<CarDto> result = carSortService.getCarsSortByPrice();
        //then
        assertThat(result).containsExactlyElementsOf(carDtos);
    }
}