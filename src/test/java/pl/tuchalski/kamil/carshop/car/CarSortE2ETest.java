package pl.tuchalski.kamil.carshop.car;

import org.json.JSONException;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.skyscreamer.jsonassert.JSONAssert;
import org.skyscreamer.jsonassert.JSONCompareMode;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.web.util.UriComponentsBuilder;

import java.io.IOException;
import java.net.URI;
import java.time.Instant;
import java.util.concurrent.TimeUnit;

import static pl.tuchalski.kamil.carshop.reader.FileReader.readFile;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@ActiveProfiles("test")
public class CarSortE2ETest {
    @Autowired
    private TestRestTemplate restTemplate;

    @Autowired
    private CarRepository carRepository;

    @LocalServerPort
    private int servicePort;

    @Test
    @DisplayName("Should return cars sorted by price")
    void shouldReturnCarsSortedByPrice() throws IOException, JSONException {
        //given
        final Instant carReceiveTime = Instant.parse("2022-01-05T23:28:56Z");
        final Car car1 = carRepository.save(new CarBuilder().withTimestamp(carReceiveTime).build());
        final Car car2 = carRepository.save(new CarBuilder().withTimestamp(carReceiveTime).witPrice(1500).build());

        final String expected = readFile("car/carsSortPrice.json")
                .replace("id1", car1.getId().toString())
                .replace("days1", String.valueOf(daysInCarShopToNow(carReceiveTime)))
                .replace("id2", car2.getId().toString());

        final URI targetUrl = UriComponentsBuilder
                .fromUriString("http://localhost:" + servicePort)
                .path("/cars")
                .queryParam("sort")
                .build()
                .encode()
                .toUri();
        //when
        final String result = restTemplate.getForObject(
                targetUrl,
                String.class
        );
        //then
        JSONAssert.assertEquals(expected, result, JSONCompareMode.NON_EXTENSIBLE);
    }

    @AfterEach
    void cleanup() {
        carRepository.deleteAll();
    }

    private long daysInCarShopToNow(final Instant date) {
        final long seconds = Instant.now().getEpochSecond() - date.getEpochSecond();
        return TimeUnit.SECONDS.toDays(seconds);
    }
}
