package pl.tuchalski.kamil.carshop.car;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class CarSortControllerTest {
    private CarSortController carSortController;

    @Mock
    private CarSortService carSortService;

    @BeforeEach
    void setup() {
        carSortController = new CarSortController(carSortService);
    }

    @Test
    @DisplayName("Should return list of cars sorted by price")
    void shouldReturnCars() {
        //given
        final List<CarDto> cars = List.of(new CarDtoBuilder().build());
        when(carSortService.getCarsSortByPrice()).thenReturn(cars);
        //when
        final List<CarDto> result = carSortController.getCarsSortByPrice();
        //then
        assertThat(result).containsExactlyElementsOf(cars);
    }
}