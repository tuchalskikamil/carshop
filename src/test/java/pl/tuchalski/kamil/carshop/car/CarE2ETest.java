package pl.tuchalski.kamil.carshop.car;


import org.json.JSONException;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.skyscreamer.jsonassert.JSONAssert;
import org.skyscreamer.jsonassert.JSONCompareMode;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.test.web.server.LocalServerPort;
import org.springframework.http.*;
import org.springframework.test.context.ActiveProfiles;
import pl.tuchalski.kamil.carshop.carPurchase.CarPurchase;
import pl.tuchalski.kamil.carshop.carPurchase.CarPurchaseRepository;
import pl.tuchalski.kamil.carshop.client.Client;
import pl.tuchalski.kamil.carshop.client.ClientRepository;

import java.io.IOException;
import java.time.Instant;
import java.util.concurrent.TimeUnit;

import static org.assertj.core.api.Assertions.assertThat;
import static pl.tuchalski.kamil.carshop.reader.FileReader.readFile;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@ActiveProfiles("test")
public class CarE2ETest {

    @Autowired
    private TestRestTemplate restTemplate;

    @Autowired
    private CarRepository carRepository;

    @Autowired
    private CarPurchaseRepository carPurchaseRepository;

    @Autowired
    private ClientRepository clientRepository;

    @LocalServerPort
    private int servicePort;

    @Test
    @DisplayName("Should return Cars")
    void shouldReturnCars() throws IOException, JSONException {
        // given
        final Instant carReceiveTime = Instant.parse("2022-01-05T23:28:56Z");
        final Car car1 = carRepository.save(new CarBuilder().withTimestamp(carReceiveTime).build());
        final Car car2 = carRepository.save(new CarBuilder().
                witBrand("Audi").
                witModel("A7").
                witProductionYear(2021).
                withSold(true).build());
        final Client client2 = clientRepository.save(new Client("Kamil", "Nowak", "123456789"));

        final CarPurchase carPurchase2 = new CarPurchase();
        carPurchase2.setTimestamp(Instant.parse("2022-01-10T15:28:56Z"));
        carPurchase2.setCar(car2);
        carPurchase2.setPrice(85000);
        carPurchase2.setClient(client2);
        carPurchaseRepository.save(carPurchase2);

        final String expected = readFile("car/cars.json")
                .replace("id1", car1.getId().toString())
                .replace("id2", car2.getId().toString())
                .replace("days1", String.valueOf(daysInCarShopToNow(carReceiveTime)));

        // when
        final String result = restTemplate.getForObject(
                "http://localhost:" + servicePort + "/cars",
                String.class
        );
        // then
        JSONAssert.assertEquals(expected, result, JSONCompareMode.NON_EXTENSIBLE);
    }

    private long daysInCarShopToNow(final Instant date) {
        final long seconds = Instant.now().getEpochSecond() - date.getEpochSecond();
        return TimeUnit.SECONDS.toDays(seconds);
    }

    @Test
    @DisplayName("Should return empty and status 204-NO CONTENT")
    void shouldReturnEmptyAndOk() throws IOException {
        // given
        final HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        final HttpEntity<String> request = new HttpEntity<>(readFile("car/car.json"), headers);
        // when
        final ResponseEntity<String> result = restTemplate.postForEntity(
                "http://localhost:" + servicePort + "/cars",
                request,
                String.class
        );
        // then
        assertThat(result.getBody()).isNullOrEmpty();
        assertThat(result.getStatusCode()).isEqualTo(HttpStatus.NO_CONTENT);
    }

    @Test
    @DisplayName("Should save car")
    void shouldSaveCar() throws IOException {
        // given
        final HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        final HttpEntity<String> request = new HttpEntity<>(readFile("car/car.json"), headers);
        // when
        restTemplate.postForEntity(
                "http://localhost:" + servicePort + "/cars",
                request,
                String.class
        );
        // then
        final Car car = carRepository.findAll().get(0);
        assertThat(carRepository.findAll().size()).isOne();
        assertThat(car.getBrand()).isEqualTo("Audi");
        assertThat(car.getModel()).isEqualTo("A7");
        assertThat(car.getProductionYear()).isEqualTo(2020);
        assertThat(car.getPrice()).isEqualTo(350000);
        assertThat(car.getId()).isNotNull();
        assertThat(car.getTimestamp()).isEqualTo(Instant.parse("2022-01-05T14:28:56Z"));
    }

    @Test
    @DisplayName("Should return status bad request")
    void shouldReturnStatusBadRequest() throws IOException, JSONException {
        // given
        final HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        final HttpEntity<String> request = new HttpEntity<>(readFile("car/carBadRequest.json"), headers);
        // when
        final ResponseEntity<String> result = restTemplate.postForEntity(
                "http://localhost:" + servicePort + "/cars",
                request,
                String.class
        );
        // then
        assertThat(result.getStatusCode()).isEqualTo(HttpStatus.BAD_REQUEST);
        JSONAssert.assertEquals(
                readFile("error/validationError.json"),
                result.getBody(),
                JSONCompareMode.NON_EXTENSIBLE
        );

    }

    @AfterEach
    void cleanup() {
        carPurchaseRepository.deleteAll();
        clientRepository.deleteAll();
        carRepository.deleteAll();
    }
}
