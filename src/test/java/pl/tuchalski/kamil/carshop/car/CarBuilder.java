package pl.tuchalski.kamil.carshop.car;

import pl.tuchalski.kamil.carshop.carPurchase.CarPurchase;

import java.time.Instant;


public class CarBuilder {
    private String brand;
    private String model;
    private int productionYear;
    private int price;
    private CarPurchase carPurchase;
    private Long id;
    private Boolean sold;
    private Instant timestamp;


    public CarBuilder() {
        this.brand = "Volkswagen";
        this.model = "Passat";
        this.productionYear = 2015;
        this.price = 85000;
        this.sold = false;
        this.timestamp = Instant.parse("2022-01-05T23:28:56Z");
    }

    public Car build() {
        return new Car(this.brand, this.model, this.productionYear, this.price, this.sold, this.timestamp);
    }

    public CarBuilder withTimestamp(final Instant timestamp) {
        this.timestamp = timestamp;
        return this;
    }

    public CarBuilder withSold(final Boolean sold) {
        this.sold = sold;
        return this;
    }

    public CarBuilder witBrand(final String brand) {
        this.brand = brand;
        return this;
    }

    public CarBuilder witModel(final String model) {
        this.model = model;
        return this;
    }

    public CarBuilder witProductionYear(final int productionYear) {
        this.productionYear = productionYear;
        return this;
    }

    public CarBuilder witPrice(final int price) {
        this.price = price;
        return this;
    }
}
