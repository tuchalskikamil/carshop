package pl.tuchalski.kamil.carshop.car;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class CarFilterControllerTest {
    @Mock
    private CarFilterService carFilterService;
    private CarFilterController carFilterController;

    @BeforeEach
    void setup() {
        carFilterController = new CarFilterController(carFilterService);
    }

    @Test
    @DisplayName("Should return list of cars filtered by brand")
    void shouldReturnListCarsOfBrand() {
        //given
        final List<CarDto> cars = List.of(
                new CarDtoBuilder().build()
        );
        when(carFilterService.getCarsFilteredByBrand("Volkswagen")).thenReturn(cars);
        //when
        List<CarDto> result = carFilterController.getCarsFilteredByBrand("Volkswagen");
        //then
        assertThat(result).containsExactlyInAnyOrderElementsOf(cars);
    }

    @Test
    @DisplayName("Should return list of cars filtered by sold")
    void shouldReturnListCarsOfSold() {
        //given
        final List<CarDto> cars = List.of(
                new CarDtoBuilder().withSold(false).build()
        );
        when(carFilterService.getCarsFilteredBySold(false)).thenReturn(cars);
        //when
        List<CarDto> result = carFilterController.getCarsFilteredBySold(false);
        //then
        assertThat(result).containsExactlyInAnyOrderElementsOf(cars);
    }

    @Test
    @DisplayName("Should return list of cars filtered by productions year")
    void shouldReturnListCarsOfProductionYear() {
        //given
        final List<CarDto> cars = List.of(
                new CarDtoBuilder().build()
        );
        when(carFilterService.getCarsFilteredByProductionYear(2014, 2020)).thenReturn(cars);
        //when
        List<CarDto> result = carFilterController.getCarsFilteredByProductionYear(2014, 2020);
        //then
        assertThat(result).containsExactlyInAnyOrderElementsOf(cars);
    }

    @Test
    @DisplayName("Should return list of cars filtered by price")
    void shouldReturnListCarsOfPrice() {
        //given
        final List<CarDto> cars = List.of(new CarDtoBuilder().build());
        when(carFilterService.getCarsFilteredByPrice(5000, 150000)).thenReturn(cars);
        //when
        List<CarDto> result = carFilterController.getCarsFilterByPrice(5000, 150000);
        //then
        assertThat(result).containsExactlyInAnyOrderElementsOf(cars);
    }
}