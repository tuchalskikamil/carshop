package pl.tuchalski.kamil.carshop.car;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.time.Instant;
import java.util.stream.Stream;

import static org.assertj.core.api.Assertions.assertThat;

class CalculatorDaysInCarShopTest {

    private final CalculatorDaysInCarShop calculatorDaysInCarShop = new CalculatorDaysInCarShop();

    private static Stream<Arguments> calculatorDaysInCarShop() {
        return Stream.of(
                Arguments.of("2022-01-25T14:28:56Z", 19),
                Arguments.of("2022-01-05T14:28:56Z", 0),
                Arguments.of("2022-01-01T14:28:56Z", -4)
        );
    }

    @DisplayName("Should return how many days the car is in the shop")
    @ParameterizedTest
    @MethodSource("calculatorDaysInCarShop")
    void shouldReturnNumberDays(final String date, final int resultDays) {
        //given
        final CarDto carDto = new CarDtoBuilder().build();
        final Instant toDate = Instant.parse(date);
        //when
        final long result = calculatorDaysInCarShop.daysInCarShopToDate(carDto, toDate);
        //then
        assertThat(result).isEqualTo(resultDays);
    }

}