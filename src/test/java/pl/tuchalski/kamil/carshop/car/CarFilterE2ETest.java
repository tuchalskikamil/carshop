package pl.tuchalski.kamil.carshop.car;


import org.json.JSONException;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.skyscreamer.jsonassert.JSONAssert;
import org.skyscreamer.jsonassert.JSONCompareMode;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.web.util.UriComponentsBuilder;

import java.io.IOException;
import java.net.URI;
import java.time.Instant;
import java.util.concurrent.TimeUnit;

import static pl.tuchalski.kamil.carshop.reader.FileReader.readFile;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@ActiveProfiles("test")
public class CarFilterE2ETest {

    @Autowired
    private TestRestTemplate restTemplate;

    @Autowired
    private CarRepository carRepository;

    @LocalServerPort
    private int servicePort;


    private long daysInCarShopToNow(final Instant date) {
        final long seconds = Instant.now().getEpochSecond() - date.getEpochSecond();
        return TimeUnit.SECONDS.toDays(seconds);
    }

    @Test
    @DisplayName("Should return cars filtered by brand")
    void shouldReturnCarsFilteredByBrand() throws IOException, JSONException {
        // given
        final Instant carReceiveTime = Instant.parse("2022-01-05T23:28:56Z");
        final Car car1 = carRepository.save(new CarBuilder().withTimestamp(carReceiveTime).build());
        carRepository.save(new CarBuilder().
                witBrand("Ferrari").
                witModel("318").
                witProductionYear(1980).
                withSold(true).build());

        final String expected = readFile("car/carsOnlyVolkswagen.json")
                .replace("id1", car1.getId().toString())
                .replace("days1", String.valueOf(daysInCarShopToNow(carReceiveTime)));

        final URI targetUrl = UriComponentsBuilder
                .fromUriString("http://localhost:" + servicePort)
                .path("/cars")
                .queryParam("brand", "Volkswagen")
                .build()
                .encode()
                .toUri();

        // when
        final String result = restTemplate.getForObject(
                targetUrl,
                String.class
        );
        // then
        JSONAssert.assertEquals(expected, result, JSONCompareMode.NON_EXTENSIBLE);
    }

    @Test
    @DisplayName("Should return cars filtered by sold")
    void shouldReturnCarsFilteredBySold() throws IOException, JSONException {
        // given
        final Instant carReceiveTime = Instant.parse("2022-01-05T23:28:56Z");
        final Car car1 = carRepository.save(new CarBuilder().withTimestamp(carReceiveTime).build());
        carRepository.save(new CarBuilder().
                witBrand("Ferrari").
                witModel("318").
                witProductionYear(1980).
                withSold(true).build());

        final String expected = readFile("car/carsOnlyVolkswagen.json")
                .replace("id1", car1.getId().toString())
                .replace("days1", String.valueOf(daysInCarShopToNow(carReceiveTime)));

        final URI targetUrl = UriComponentsBuilder
                .fromUriString("http://localhost:" + servicePort)
                .path("/cars")
                .queryParam("sold", false)
                .build()
                .encode()
                .toUri();

        // when
        final String result = restTemplate.getForObject(
                targetUrl,
                String.class
        );
        // then
        JSONAssert.assertEquals(expected, result, JSONCompareMode.NON_EXTENSIBLE);
    }

    @Test
    @DisplayName("Should return cars filtered by productions yar")
    void shouldReturnCarsFilteredByProductionYear() throws IOException, JSONException {
        // given
        final Instant carReceiveTime = Instant.parse("2022-01-05T23:28:56Z");
        final Car car1 = carRepository.save(new CarBuilder().withTimestamp(carReceiveTime).build());
        carRepository.save(new CarBuilder().
                witBrand("Ferrari").
                witModel("318").
                witProductionYear(1980).
                withSold(true).build());

        final String expected = readFile("car/carsOnlyVolkswagen.json")
                .replace("id1", car1.getId().toString())
                .replace("days1", String.valueOf(daysInCarShopToNow(carReceiveTime)));

        final URI targetUrl = UriComponentsBuilder
                .fromUriString("http://localhost:" + servicePort)
                .path("/cars")
                .queryParam("minProductionYear", 2014)
                .queryParam("maxProductionYear", 2020)
                .build()
                .encode()
                .toUri();

        // when
        final String result = restTemplate.getForObject(
                targetUrl,
                String.class
        );
        // then
        JSONAssert.assertEquals(expected, result, JSONCompareMode.NON_EXTENSIBLE);
    }

    @Test
    @DisplayName("Should return cars filtered by price")
    void shouldReturnCarsFilteredByPrice() throws IOException, JSONException {
        // given
        final Instant carReceiveTime = Instant.parse("2022-01-05T23:28:56Z");
        final Car car1 = carRepository.save(new CarBuilder().withTimestamp(carReceiveTime).build());
        carRepository.save(new CarBuilder()
                .witBrand("Ferrari")
                .witModel("318")
                .witProductionYear(1980)
                .witPrice(1500)
                .withSold(true).build());

        final String expected = readFile("car/carsOnlyVolkswagen.json")
                .replace("id1", car1.getId().toString())
                .replace("days1", String.valueOf(daysInCarShopToNow(carReceiveTime)));

        final URI targetUrl = UriComponentsBuilder
                .fromUriString("http://localhost:" + servicePort)
                .path("/cars")
                .queryParam("minPrice", 3000)
                .queryParam("maxPrice", 150000)
                .build()
                .encode()
                .toUri();

        // when
        final String result = restTemplate.getForObject(
                targetUrl,
                String.class
        );
        // then
        JSONAssert.assertEquals(expected, result, JSONCompareMode.NON_EXTENSIBLE);
    }

    @AfterEach
    void cleanup() {
        carRepository.deleteAll();
    }
}
