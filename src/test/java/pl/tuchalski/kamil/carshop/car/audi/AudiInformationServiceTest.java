package pl.tuchalski.kamil.carshop.car.audi;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;

import static org.assertj.core.api.Assertions.assertThat;


@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@ActiveProfiles("test")
class AudiInformationServiceTest {

    @Autowired
    private AudiInformationService audiInformationService;

    @Test
    @DisplayName("Should return Audi car")
    void shouldReturnCar() {
        //given
        //when
        final CarAudi result = audiInformationService.fetchAudiInformation("A7");
        //then
        assertThat(result).isEqualTo(new CarAudi("Audi", "A7", "white", "4", "sport", 6L));
    }
}