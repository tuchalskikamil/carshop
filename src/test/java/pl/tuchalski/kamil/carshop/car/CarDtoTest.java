package pl.tuchalski.kamil.carshop.car;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;
import java.util.Set;
import java.util.stream.Stream;

import static org.assertj.core.api.Assertions.assertThat;

class CarDtoTest {
    private ValidatorFactory validatorFactory;
    private Validator validator;

    public static Stream<Arguments> getCars() {
        return Stream.of(
                Arguments.of(new CarDtoBuilder().withBrand("").build(), 1),
                Arguments.of(new CarDtoBuilder().withBrand(" ").build(), 1),
                Arguments.of(new CarDtoBuilder().withBrand(null).build(), 1),
                Arguments.of(new CarDtoBuilder().withModel("").build(), 1),
                Arguments.of(new CarDtoBuilder().withModel(" ").build(), 1),
                Arguments.of(new CarDtoBuilder().withModel(null).build(), 1),
                Arguments.of(new CarDtoBuilder().withProductionYear(1889).build(), 1),
                Arguments.of(new CarDtoBuilder().withProductionYear(2051).build(), 1),
                Arguments.of(new CarDtoBuilder().withPrice(-1).build(), 1),
                Arguments.of(new CarDtoBuilder().withTimestamp(null).build(), 1),

                Arguments.of(new CarDtoBuilder().withBrand("!").build(), 0),
                Arguments.of(new CarDtoBuilder().withModel("A").build(), 0),
                Arguments.of(new CarDtoBuilder().withModel("!").build(), 0),
                Arguments.of(new CarDtoBuilder().withProductionYear(1900).build(), 0),
                Arguments.of(new CarDtoBuilder().withProductionYear(2050).build(), 0),
                Arguments.of(new CarDtoBuilder().build(), 0),
                Arguments.of(new CarDtoBuilder().withPrice(0).build(), 0),
                Arguments.of(new CarDtoBuilder().withPrice(1580000000).build(), 0)
                );
    }

    @BeforeEach
    public void createValidator() {
        validatorFactory = Validation.buildDefaultValidatorFactory();
        validator = validatorFactory.getValidator();
    }

    @AfterEach
    public void close() {
        validatorFactory.close();
    }

    @DisplayName("Should validate")
    @ParameterizedTest(name = "Should return {1} violations for {0}")
    @MethodSource("getCars")
    void shouldValidate(final CarDto invalidCar, final int violationsNumber) {
        //given
        // when
        final Set<ConstraintViolation<CarDto>> violations = validator.validate(invalidCar);
        //then
        assertThat(violations).hasSize(violationsNumber);
    }
}
