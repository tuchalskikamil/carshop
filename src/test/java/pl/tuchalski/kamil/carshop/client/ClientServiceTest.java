package pl.tuchalski.kamil.carshop.client;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.modelmapper.ModelMapper;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class ClientServiceTest {

    private ClientService clientService;

    @Mock
    private ClientRepository clientRepository;
    @Mock
    private ModelMapper modelMapper;

    @BeforeEach
    void setup() {
        clientService = new ClientService(clientRepository, modelMapper);
    }

    @Test
    @DisplayName("Should return list of Clients")
    void shouldReturnListOfClients() {
        //given
        final ClientDto clientDto1 = new ClientDto(1L, "Kamil", "Nowak", "123456789");
        final ClientDto clientDto2 = new ClientDto(2L, "Dorota", "Skarbek", "999888777");
        final List<ClientDto> clientDtos = List.of(clientDto1, clientDto2);
        final Client client1 = new Client("Kamil", "Nowak", "123456789");
        final Client client2 = new Client("Dorota", "Skarbek", "999888777");
        final List<Client> clients = List.of(client1, client2);
        when(clientRepository.findAll()).thenReturn(clients);
        when(modelMapper.map(client1, ClientDto.class)).thenReturn(clientDto1);
        when(modelMapper.map(client2, ClientDto.class)).thenReturn(clientDto2);
        //when
        final List<ClientDto> result = clientService.getAllClients();
        //then
        assertThat(result).containsExactlyInAnyOrderElementsOf(clientDtos);
    }

    @Test
    @DisplayName("Should verify save")
    void shouldVerifySave() {
        //given
        final Client client = new Client();
        final ClientDto clientDto = new ClientDto(1L, "Dorota", "Skarbek", "999888777");
        when(modelMapper.map(clientDto, Client.class)).thenReturn(client);
        //when
        clientService.addClient(clientDto);
        //then
        verify(clientRepository).save(client);
    }
}
