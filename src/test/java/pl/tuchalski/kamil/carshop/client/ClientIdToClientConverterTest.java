package pl.tuchalski.kamil.carshop.client;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.modelmapper.spi.MappingContext;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class ClientIdToClientConverterTest {

    private final ClientIdToClientConverter clientIdToClientConverter = new ClientIdToClientConverter();

    @Mock
    private MappingContext<Long, Client> context;

    @Test
    @DisplayName("Should return convert client")
    void shouldReturnConvertClient() {
        //given
        final Client client = new Client();
        client.setId(1L);
        when(context.getSource()).thenReturn(1L);
        //when
        final Client result = clientIdToClientConverter.convert(context);
        //then
        assertThat(result).isEqualTo(client);
    }
}