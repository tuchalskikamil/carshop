package pl.tuchalski.kamil.carshop.client;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;
import java.util.Set;
import java.util.stream.Stream;

import static org.assertj.core.api.Assertions.assertThat;

class ClientDtoTest {
    private ValidatorFactory validatorFactory;
    private Validator validator;

    public static Stream<Arguments> getClients() {
        return Stream.of(
                Arguments.of(new ClientDto(1L, "", "Kowalski", "123456789"), 1),
                Arguments.of(new ClientDto(1L, " ", "Kowalski", "123456789"), 1),
                Arguments.of(new ClientDto(1L, "Jan", "", "123456789"), 1),
                Arguments.of(new ClientDto(1L, "Jan", " ", "123456789"), 1),
                Arguments.of(new ClientDto(1L, "Jan", "Kowalski", ""), 1),
                Arguments.of(new ClientDto(1L, "Jan", "Kowalski", "12345678"), 1),
                Arguments.of(new ClientDto(1L, "Jan", "Kowalski", "1234567890"), 1),
                Arguments.of(new ClientDto(1L, "Jan", "Kowalski", "A23456789"), 1),
                Arguments.of(new ClientDto(1L, "Jan", "Kowalski", " 23456789"), 1),
                Arguments.of(new ClientDto(1L, "Jan", "Kowalski", "!23456789"), 1),

                Arguments.of(new ClientDto(1L, "A", "Kowalski", "123456789"), 0),
                Arguments.of(new ClientDto(1L, "Jan", "K", "123456789"), 0),
                Arguments.of(new ClientDto(1L, "Jan", "Kowalski", "123456789"), 0)
        );
    }

    @BeforeEach
    public void createValidator() {
        validatorFactory = Validation.buildDefaultValidatorFactory();
        validator = validatorFactory.getValidator();
    }

    @AfterEach
    public void close() {
        validatorFactory.close();
    }

    @DisplayName("Should validate")
    @ParameterizedTest(name = "Should return {1} violations for {0}")
    @MethodSource("getClients")
    void shouldValidate(final ClientDto invalidClient, final int violationsNumber) {
        //given
        // when
        final Set<ConstraintViolation<ClientDto>> violations = validator.validate(invalidClient);
        //then
        assertThat(violations).hasSize(violationsNumber);
    }
}
