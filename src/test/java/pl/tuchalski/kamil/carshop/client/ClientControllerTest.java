package pl.tuchalski.kamil.carshop.client;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class ClientControllerTest {
    @Mock
    private ClientService clientService;
    private ClientController clientController;


    @BeforeEach
    void setup() {
        clientController = new ClientController(clientService, "client");
    }

    @Test
    @DisplayName("Should return list of Clients")
    void shouldReturnListOfClients() {
        //given
        final List<ClientDto> clients = List.of(
                new ClientDto(1L, "Kamil", "Kowalski", "555-444-222"),
                new ClientDto(2L, "Dorota", "Nowak", "111-222-333")
        );
        when(clientService.getAllClients()).thenReturn(clients);

        //when
        final List<ClientDto> result = clientController.getAllClients();

        //then
        assertThat(result).containsExactlyInAnyOrderElementsOf(clients);
    }

    @Test
    @DisplayName("Should return empty body")
    void shouldReturnEmptyBody() {
        //given
        final ClientDto clientDto = new ClientDto(1L, "Kamil", "Kowalski", "555-444-222");
        //when
        final ResponseEntity<String> result = clientController.addClient(clientDto);
        //then
        assertThat(result.getBody()).isNull();
    }

    @Test
    @DisplayName("Should return status 204-NO CONTENT")
    void shouldVerifySave() {
        //given
        final ClientDto clientDto = new ClientDto(1L, "Kamil", "Kowalski", "555-444-222");
        // when
        final ResponseEntity<String> result = clientController.addClient(clientDto);
        //then
        assertThat(result.getStatusCode()).isEqualTo(HttpStatus.NO_CONTENT);
    }
}
