package pl.tuchalski.kamil.carshop.client;

import org.json.JSONException;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.skyscreamer.jsonassert.JSONAssert;
import org.skyscreamer.jsonassert.JSONCompareMode;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.test.web.server.LocalServerPort;
import org.springframework.http.*;
import org.springframework.test.context.ActiveProfiles;

import java.io.IOException;

import static org.assertj.core.api.Assertions.assertThat;
import static pl.tuchalski.kamil.carshop.reader.FileReader.readFile;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@ActiveProfiles("test")
public class Client2E2Test {

    @Autowired
    private TestRestTemplate restTemplate;

    @Autowired
    private ClientRepository clientRepository;

    @LocalServerPort
    private int servicePort;

    @Test
    @DisplayName("Should return Client")
    void shouldReturnClient() throws IOException, JSONException {
        // given
        final Client client = clientRepository.save(new Client("Kamil", "Kowalski", "123456789"));
        final Client client2 = clientRepository.save(new Client("Dorota", "Nowak", "987654321"));
        final String expected = readFile("client/clients.json")
                .replace("id1", client.getId().toString())
                .replace("id2", client2.getId().toString());
        //when
        final String result = restTemplate.getForObject(
                "http://localhost:" + servicePort + "/clients",
                String.class
        );
        //then
        JSONAssert.assertEquals(expected, result, JSONCompareMode.NON_EXTENSIBLE);
    }

    @Test
    @DisplayName("Should return empty and status 204-NO CONTENT")
    void shouldReturnEmptyAndOk() throws IOException {
        //given
        final HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        final HttpEntity<String> request = new HttpEntity<>(readFile("client/client.json"), headers);
        //when
        final ResponseEntity<String> result = restTemplate.postForEntity(
                "http://localhost:" + servicePort + "/clients",
                request,
                String.class
        );
        //then
        assertThat(result.getBody()).isNullOrEmpty();
        assertThat(result.getStatusCode()).isEqualTo(HttpStatus.NO_CONTENT);
    }

    @Test
    @DisplayName("Should save client")
    void shouldSaveClient() throws IOException {
        //given
        final HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        final HttpEntity<String> request = new HttpEntity<>(readFile("client/client.json"), headers);
        //when
        restTemplate.postForEntity(
                "http://localhost:" + servicePort + "/clients",
                request,
                String.class
        );
        //then
        final Client client = clientRepository.findAll().get(0);
        assertThat(clientRepository.findAll().size()).isOne();
        assertThat(client.getFirstName()).isEqualTo("Kamil");
        assertThat(client.getLastName()).isEqualTo("Kowalski");
        assertThat(client.getPhoneNumber()).isEqualTo("123456789");
        assertThat(client.getId()).isNotNull();
    }

    @Test
    @DisplayName("Should return status bad request")
    void shouldReturnStatusBadRequest() throws IOException, JSONException {
        //given
        final HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        final HttpEntity<String> request = new HttpEntity<>(readFile("client/clientBadRequest.json"), headers);
        //when
        final ResponseEntity<String> result = restTemplate.postForEntity(
                "http://localhost:" + servicePort + "/clients",
                request,
                String.class
        );
        //then
        assertThat(result.getStatusCode()).isEqualTo(HttpStatus.BAD_REQUEST);
        JSONAssert.assertEquals(
                readFile("error/validationError.json"),
                result.getBody(),
                JSONCompareMode.NON_EXTENSIBLE
        );
    }

    @AfterEach
    void cleanup() {
        clientRepository.deleteAll();
    }
}
