package pl.tuchalski.kamil.carshop.carPurchase;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.time.Instant;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class CarPurchaseControllerTest {
    private CarPurchaseController carPurchaseController;
    @Mock
    private CarPurchaseService carPurchaseService;

    @BeforeEach
    void setup() {
        carPurchaseController = new CarPurchaseController(carPurchaseService);
    }

    @Test
    @DisplayName("Should return list of cars purchases")
    void shouldReturnListOfCarsPurchases() {
        //given
        final List<CarPurchaseDto> purchases = List.of(
                new CarPurchaseDto(1L, Instant.now(), 150000, 1L, 1L, 1L),
                new CarPurchaseDto(2L, Instant.now(), 500, 2L, 3L, 2L)
        );
        when(carPurchaseService.getAllPurchase()).thenReturn(purchases);
        //when
        final List<CarPurchaseDto> result = carPurchaseController.getAllPurchases();
        //then
        assertThat(result).containsExactlyInAnyOrderElementsOf(purchases);
    }

    @Test
    @DisplayName("Should return status 204-NO CONTENT")
    void shouldReturnStatusOk() {
        //given
        final CarPurchaseDto carPurchaseDto = new CarPurchaseDto(1L, Instant.now(), 150000, 1L, 1L, 1L);
        //when
        final ResponseEntity<String> result = carPurchaseController.addPurchase(carPurchaseDto);
        //then
        assertThat(result.getStatusCode()).isEqualTo(HttpStatus.NO_CONTENT);
    }

    @Test
    @DisplayName("Should return empty body")
    void shouldReturnEmptyBody() {
        //given
        final CarPurchaseDto carPurchaseDto = new CarPurchaseDto(1L, Instant.now(), 150000, 1L, 1L, 1L);
        //when
        final ResponseEntity<String> result = carPurchaseController.addPurchase(carPurchaseDto);
        //then
        assertThat(result.getBody()).isNull();
    }
}