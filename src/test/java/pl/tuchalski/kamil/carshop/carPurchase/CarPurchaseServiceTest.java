package pl.tuchalski.kamil.carshop.carPurchase;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.modelmapper.ModelMapper;
import pl.tuchalski.kamil.carshop.car.Car;
import pl.tuchalski.kamil.carshop.car.CarRepository;

import java.time.Instant;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class CarPurchaseServiceTest {

    private CarPurchaseService carPurchaseService;
    @Mock
    private CarPurchaseRepository carPurchaseRepository;
    @Mock
    private ModelMapper modelMapper;
    @Mock
    private CarRepository carRepository;

    @BeforeEach
    void setup() {
        carPurchaseService = new CarPurchaseService(carPurchaseRepository, modelMapper, carRepository);
    }

    @Test
    @DisplayName("Should return list of cars purchases")
    void shouldReturnListOfCarsPurchases() {
        //given
        final CarPurchaseDto carPurchaseDto1 = new CarPurchaseDto(1L, Instant.now(), 150000, 1L, 1L, 1L);
        final CarPurchaseDto carPurchaseDto2 = new CarPurchaseDto(2L, Instant.now(), 500, 2L, 3L, 2L);
        final List<CarPurchaseDto> carPurchaseDtos = List.of(carPurchaseDto1, carPurchaseDto2);
        final CarPurchase carPurchase1 = new CarPurchase();
        carPurchase1.setId(1L);
        final CarPurchase carPurchase2 = new CarPurchase();
        carPurchase2.setId(2L);
        final List<CarPurchase> carPurchases = List.of(carPurchase1, carPurchase2);
        when(carPurchaseRepository.findAll()).thenReturn(carPurchases);
        when(modelMapper.map(carPurchase1, CarPurchaseDto.class)).thenReturn(carPurchaseDto1);
        when(modelMapper.map(carPurchase2, CarPurchaseDto.class)).thenReturn(carPurchaseDto2);
        //when
        final List<CarPurchaseDto> result = carPurchaseService.getAllPurchase();
        //then
        assertThat(result).containsExactlyInAnyOrderElementsOf(carPurchaseDtos);
    }

    @Test
    @DisplayName("Should verify save")
    void shouldVerifySave() {
        //given
        final CarPurchase carPurchase = new CarPurchase();
        final CarPurchaseDto carPurchaseDto = new CarPurchaseDto(1L, Instant.now(), 150000, 1L, 1L, 1L);
        when(modelMapper.map(carPurchaseDto, CarPurchase.class)).thenReturn(carPurchase);
        when(carRepository.getById(1L)).thenReturn(new Car());
        //when
        carPurchaseService.addCarPurchase(carPurchaseDto);
        //then
        verify(carPurchaseRepository).save(carPurchase);
    }

    @Test
    @DisplayName("Should set to sold")
    void shouldSetToSold() {
        //given
        final CarPurchase carPurchase = new CarPurchase();
        final CarPurchaseDto carPurchaseDto = new CarPurchaseDto(1L, Instant.now(), 150000, 1L, 1L, 1L);
        when(modelMapper.map(carPurchaseDto, CarPurchase.class)).thenReturn(carPurchase);
        Car car = new Car();
        car.setSold(false);
        when(carRepository.getById(1L)).thenReturn(car);
        //when
        carPurchaseService.addCarPurchase(carPurchaseDto);
        //then
        assertThat(car.getSold()).isEqualTo(true);
    }
}