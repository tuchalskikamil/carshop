package pl.tuchalski.kamil.carshop.carPurchase;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;
import java.time.Instant;
import java.util.Set;
import java.util.stream.Stream;

import static org.assertj.core.api.Assertions.assertThat;

class CarPurchaseDtoTest {
    private ValidatorFactory validatorFactory;
    private Validator validator;

    public static Stream<Arguments> getCarPurchases() {
        return Stream.of(
                Arguments.of(new CarPurchaseDto(1L, Instant.now(), 150000, 1L, 1L, 1L), 0),
                Arguments.of(new CarPurchaseDto(1L, Instant.now(), 0, 1L, 1L, 1L), 0),

                Arguments.of(new CarPurchaseDto(1L, null, 150000, 1L, 1L, 1L), 1),
                Arguments.of(new CarPurchaseDto(1L, Instant.now(), -50, 1L, 1L, 1L), 1),
                Arguments.of(new CarPurchaseDto(1L, Instant.now(), 150000, null, 1L, 1L), 1),
                Arguments.of(new CarPurchaseDto(1L, Instant.now(), 150000, 1L, null, 1L), 1),
                Arguments.of(new CarPurchaseDto(1L, Instant.now(), 150000, 1L, 1L, null), 1)
        );
    }

    @BeforeEach
    public void createValidator() {
        validatorFactory = Validation.buildDefaultValidatorFactory();
        validator = validatorFactory.getValidator();
    }

    @AfterEach
    public void close() {
        validatorFactory.close();
    }

    @DisplayName("Should validate")
    @ParameterizedTest(name = "Should return {1} violations for {0}")
    @MethodSource("getCarPurchases")
    void shouldValidate(final CarPurchaseDto invalidCarPurchase, final int violationsNumber) {
        //given
        // when
        final Set<ConstraintViolation<CarPurchaseDto>> violations = validator.validate(invalidCarPurchase);
        //then
        assertThat(violations).hasSize(violationsNumber);
    }

}