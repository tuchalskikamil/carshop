package pl.tuchalski.kamil.carshop.carPurchase;

import org.json.JSONException;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.skyscreamer.jsonassert.JSONAssert;
import org.skyscreamer.jsonassert.JSONCompareMode;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.*;
import org.springframework.test.context.ActiveProfiles;
import pl.tuchalski.kamil.carshop.car.Car;
import pl.tuchalski.kamil.carshop.car.CarRepository;
import pl.tuchalski.kamil.carshop.client.Client;
import pl.tuchalski.kamil.carshop.client.ClientRepository;
import pl.tuchalski.kamil.carshop.carDealer.CarDealer;
import pl.tuchalski.kamil.carshop.carDealer.CarDealerRepository;

import java.io.IOException;
import java.time.Instant;

import static org.assertj.core.api.Assertions.assertThat;
import static pl.tuchalski.kamil.carshop.reader.FileReader.readFile;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@ActiveProfiles("test")
public class CarPurchaseE2ETest {

    @Autowired
    private TestRestTemplate restTemplate;

    @Autowired
    private CarPurchaseRepository carPurchaseRepository;

    @Autowired
    private CarRepository carRepository;

    @Autowired
    private ClientRepository clientRepository;

    @Autowired
    private CarDealerRepository carDealerRepository;

    @LocalServerPort
    private int servicePort;
    private Car car;
    private Client client;
    private CarDealer carDealer;

    @BeforeEach
    void setup() {
        car = carRepository.save(new Car("Audi", "A4", 2020, 150000, false, null));
        client = clientRepository.save(new Client("Lilianna", "Usmiech", "123456789"));
        carDealer = carDealerRepository.save(new CarDealer("Kamil", "Nowak", "nowak@gmail.com"));
    }

    @Test
    @DisplayName("Should return cars purchases")
    void shouldReturnCarsPurchases() {
        // given
        carPurchaseRepository.save(getCarPurchase());
        // when
        final CarPurchaseDto[] result = restTemplate.getForObject(
                "http://localhost:" + servicePort + "/purchases",
                CarPurchaseDto[].class
        );
        // then
        assertThat(result[0].getCarId()).isEqualTo(car.getId());
        assertThat(result[0].getClientId()).isEqualTo(client.getId());
        assertThat(result[0].getCarDealerId()).isEqualTo(carDealer.getId());
        assertThat(result[0].getId()).isNotNull();
        assertThat(result[0].getPrice()).isEqualTo(140000);
        assertThat(result[0].getTimestamp()).isEqualTo("2022-01-05T14:28:56Z");
    }

    @Test
    @DisplayName("Should return empty and status 204-NO CONTENT")
    void shouldReturnEmptyAndOk() {
        // given
        final HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        final HttpEntity<String> request = new HttpEntity<>(carPurchaseToSave(), headers);
        // when
        final ResponseEntity<String> result = restTemplate.postForEntity(
                "http://localhost:" + servicePort + "/purchases",
                request,
                String.class
        );
        // then
        assertThat(result.getBody()).isNullOrEmpty();
        assertThat(result.getStatusCode()).isEqualTo(HttpStatus.NO_CONTENT);
    }

    @Test
    @DisplayName("Should save car purchase")
    void shouldSaveCarPurchase() throws IOException {
        // given
        final HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        final HttpEntity<String> request = new HttpEntity<>(carPurchaseToSave(), headers);
        // when
        restTemplate.postForEntity(
                "http://localhost:" + servicePort + "/purchases",
                request,
                String.class
        );
        // then
        final CarPurchase carPurchase = carPurchaseRepository.findAll().get(0);
        assertThat(carPurchaseRepository.findAll().size()).isOne();
        assertThat(carPurchase.getCar().getId()).isEqualTo(car.getId());
        assertThat(carPurchase.getCar().getSold()).isTrue();
        assertThat(carPurchase.getClient().getId()).isEqualTo(client.getId());
        assertThat(carPurchase.getCarDealer().getId()).isEqualTo(carDealer.getId());
        assertThat(carPurchase.getPrice()).isEqualTo(140000);
        assertThat(carPurchase.getTimestamp()).isEqualTo("2022-01-05T14:28:56Z");
    }

    @Test
    @DisplayName("Should return status bad request")
    void shouldReturnStatusBadRequest() throws IOException, JSONException {
        // given
        final HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        final HttpEntity<String> request = new HttpEntity<>(readFile("purchase/purchaseBadRequest.json"), headers);
        // when
        final ResponseEntity<String> result = restTemplate.postForEntity(
                "http://localhost:" + servicePort + "/purchases",
                request,
                String.class
        );
        // then
        assertThat(result.getStatusCode()).isEqualTo(HttpStatus.BAD_REQUEST);
        JSONAssert.assertEquals(
                readFile("error/validationError.json"),
                result.getBody(),
                JSONCompareMode.NON_EXTENSIBLE
        );

    }

    @AfterEach
    void cleanup() {
        carPurchaseRepository.deleteAll();
        clientRepository.deleteAll();
        carRepository.deleteAll();
        carDealerRepository.deleteAll();
    }

    private CarPurchase getCarPurchase() {
        CarPurchase carPurchase = new CarPurchase();
        carPurchase.setCar(car);
        carPurchase.setClient(client);
        carPurchase.setUser(carDealer);
        carPurchase.setPrice(140000);
        carPurchase.setTimestamp(Instant.parse("2022-01-05T14:28:56Z"));
        return carPurchase;
    }

    private String carPurchaseToSave() {
        return "  {" +
                "    \"carId\": " + car.getId() + "," +
                "    \"clientId\": " + client.getId() + "," +
                "    \"carDealerId\": " + carDealer.getId() + "," +
                "    \"timestamp\": \"2022-01-05T14:28:56Z\"," +
                "    \"price\": 140000" +
                "  }";
    }

}
