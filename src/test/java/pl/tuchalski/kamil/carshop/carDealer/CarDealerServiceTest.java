package pl.tuchalski.kamil.carshop.carDealer;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.modelmapper.ModelMapper;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class CarDealerServiceTest {
    private CarDealerService carDealerService;

    @Mock
    private CarDealerRepository carDealerRepository;
    @Mock
    private ModelMapper modelMapper;

    @BeforeEach
    void setup() {
        carDealerService = new CarDealerService(carDealerRepository, modelMapper);
    }

    @Test
    @DisplayName("Should return list of car dealer")
    void shouldReturnListOfCarDealer() {
        //given
        final CarDealerDto carDealerDto1 = new CarDealerDto("Kamil", "Nowak", "nowak@gmail.com", 1L);
        final CarDealerDto carDealerDto2 = new CarDealerDto("Dorota", "Kowalska", "kowalska@gmail.com", 2L);
        final List<CarDealerDto> carDealerDtos = List.of(carDealerDto1, carDealerDto2);
        final CarDealer carDealer1 = new CarDealer("Kamil", "Nowak", "nowak@gmail.com");
        final CarDealer carDealer2 = new CarDealer("Dorota", "Kowalska", "kowalska@gmail.com");
        final List<CarDealer> carDealer = List.of(carDealer1, carDealer2);
        when(carDealerRepository.findAll()).thenReturn(carDealer);
        when(modelMapper.map(carDealer1, CarDealerDto.class)).thenReturn(carDealerDto1);
        when(modelMapper.map(carDealer2, CarDealerDto.class)).thenReturn(carDealerDto2);
        //when
        final List<CarDealerDto> result = carDealerService.getAllCarDealer();
        //then
        assertThat(result).containsExactlyInAnyOrderElementsOf(carDealerDtos);
    }

    @Test
    @DisplayName("Should verify save")
    void shouldVerifySave() {
        //given
        final CarDealer carDealer = new CarDealer();
        final CarDealerDto carDealerDto = new CarDealerDto();
        when(modelMapper.map(carDealerDto, CarDealer.class)).thenReturn(carDealer);
        //when
        carDealerService.addCarDealer(carDealerDto);
        //then
        verify(carDealerRepository).save(carDealer);
    }
}