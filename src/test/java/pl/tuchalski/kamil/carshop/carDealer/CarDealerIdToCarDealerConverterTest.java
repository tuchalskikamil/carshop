package pl.tuchalski.kamil.carshop.carDealer;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.modelmapper.spi.MappingContext;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class CarDealerIdToCarDealerConverterTest {

    private final CarDealerIdToCarDealerConverter carDealerIdToCarDealerConverter = new CarDealerIdToCarDealerConverter();

    @Mock
    private MappingContext<Long, CarDealer> context;

    @Test
    @DisplayName("Should return convert car dealer")
    void shouldReturnConvertCarDealer() {
        //given
        final CarDealer carDealer = new CarDealer();
        carDealer.setId(1L);
        when(context.getSource()).thenReturn(1L);
        //when
        final CarDealer result = carDealerIdToCarDealerConverter.convert(context);
        //then
        assertThat(result).isEqualTo(carDealer);
    }
}