package pl.tuchalski.kamil.carshop.carDealer;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.util.List;

import static org.assertj.core.api.AssertionsForInterfaceTypes.assertThat;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class CarDealerControllerTest {
    @Mock
    private CarDealerService carDealerService;
    private CarDealerController carDealerController;

    @BeforeEach
    void setup() {
        carDealerController = new CarDealerController(carDealerService);
    }

    @Test
    @DisplayName("Should return list of car dealers")
    void shouldReturnListOfCarDealers() {
        //given
        final List<CarDealerDto> carDealers = List.of(
                new CarDealerDto("Kamil", "Nowak", "nowak@gmail.com", 1L),
                new CarDealerDto("Dorota", "Kowalska", "kowalska@gmail.com", 2L)
        );
        when(carDealerService.getAllCarDealer()).thenReturn(carDealers);
        //when
        final List<CarDealerDto> result = carDealerController.getAllCarDealer();
        //then
        assertThat(result).containsExactlyInAnyOrderElementsOf(carDealers);
    }

    @Test
    @DisplayName("Should return status 204-NO CONTENT")
    void shouldVeritySave() {
        //given
        final CarDealerDto carDealerDto = new CarDealerDto("Kamil", "Nowak", "nowak@gmail.com", 1L);
        //when
        final ResponseEntity<String> result = carDealerController.addCarDealer(carDealerDto);
        //then
        assertThat(result.getStatusCode()).isEqualTo(HttpStatus.NO_CONTENT);
    }
}