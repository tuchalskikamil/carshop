package pl.tuchalski.kamil.carshop.carDealer;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;
import java.util.Set;
import java.util.stream.Stream;

import static org.assertj.core.api.Assertions.assertThat;

class CarDealerDtoTest {
    private ValidatorFactory validatorFactory;
    private Validator validator;

    public static Stream<Arguments> getCarDealers() {
        return Stream.of(
                Arguments.of(new CarDealerDto("Kamil", "Nowak", "nowak@gmail.com", 1L), 0),
                Arguments.of(new CarDealerDto("Kamil", "Nowak", "nowak.kamil@gmail.com", 1L), 0),

                Arguments.of(new CarDealerDto("", "Nowak", "nowak@gmail.com", 1L), 1),
                Arguments.of(new CarDealerDto("Kamil", "", "nowak@gmail.com", 1L), 1),
                Arguments.of(new CarDealerDto("Kamil", "Nowak", "", 1L), 1),
                Arguments.of(new CarDealerDto("Kamil", "Nowak", "nowakgmail.com", 1L), 1),
                Arguments.of(new CarDealerDto("Kamil", "Nowak", "nowak@gmailcom", 1L), 1),
                Arguments.of(new CarDealerDto("Kamil", "Nowak", "nowak@gmail@g.com", 1L), 1)
        );
    }

    @BeforeEach
    public void createValidator() {
        validatorFactory = Validation.buildDefaultValidatorFactory();
        validator = validatorFactory.getValidator();
    }

    @AfterEach
    public void close() {
        validatorFactory.close();
    }

    @DisplayName("Should validate")
    @ParameterizedTest(name = "Should return {1} violations for {0}")
    @MethodSource("getCarDealers")
    void shouldValidate(final CarDealerDto invalidCarDealer, final int violationsNumber) {
        //given
        //when
        final Set<ConstraintViolation<CarDealerDto>> violations = validator.validate(invalidCarDealer);
        //then
        assertThat(violations).hasSize(violationsNumber);
    }
}