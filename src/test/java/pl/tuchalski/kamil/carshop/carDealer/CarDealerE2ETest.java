package pl.tuchalski.kamil.carshop.carDealer;

import org.json.JSONException;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.skyscreamer.jsonassert.JSONAssert;
import org.skyscreamer.jsonassert.JSONCompareMode;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.*;
import org.springframework.test.context.ActiveProfiles;

import java.io.IOException;

import static org.assertj.core.api.Assertions.assertThat;
import static pl.tuchalski.kamil.carshop.reader.FileReader.readFile;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@ActiveProfiles("test")
public class CarDealerE2ETest {

    @Autowired
    private TestRestTemplate restTemplate;
    @Autowired
    private CarDealerRepository carDealerRepository;
    @LocalServerPort
    private int servicePort;

    @Test
    @DisplayName("Should return  car dealer")
    void shouldReturnCarDealer() throws IOException, JSONException{
        //given
        final CarDealer carDealer1 = carDealerRepository.save( new CarDealer("Kamil", "Nowak", "nowak@gmail.com"));
        final CarDealer carDealer2 = carDealerRepository.save(new CarDealer("Dorota", "Kowalska", "kowalska@gmail.com"));
        final String expected = readFile("carDealer/carDealers.json")
                .replace("id1", carDealer1.getId().toString())
                .replace("id2", carDealer2.getId().toString());
        //when
        final String result = restTemplate.getForObject(
                "http://localhost:" + servicePort + "/carDealer",
                String.class
        );
        //then
        JSONAssert.assertEquals(expected, result, JSONCompareMode.NON_EXTENSIBLE);
    }

    @Test
    @DisplayName("Should return empty and status 204-NO CONTENT")
    void shouldReturnEmptyAndOk() throws IOException {
        //given
        final HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        final HttpEntity<String> request = new HttpEntity<>(readFile("carDealer/carDealer.json"), headers);
        //when
        final ResponseEntity<String> result = restTemplate.postForEntity(
                "http://localhost:" + servicePort + "/carDealer",
                request,
                String.class
        );
        //then
        assertThat(result.getBody()).isNullOrEmpty();
        assertThat(result.getStatusCode()).isEqualTo(HttpStatus.NO_CONTENT);
    }

    @Test
    @DisplayName("Should save car dealer")
    void shouldSaveCarDealer() throws IOException {
        //given
        final HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        final HttpEntity<String> request = new HttpEntity<>(readFile("carDealer/carDealer.json"), headers);
        //when
        restTemplate.postForEntity(
                "http://localhost:" + servicePort + "/carDealer",
                request,
                String.class
        );
        //then
        final CarDealer carDealer = carDealerRepository.findAll().get(0);
        assertThat(carDealerRepository.findAll().size()).isOne();
        assertThat(carDealer.getFirstName()).isEqualTo("Kamil");
        assertThat(carDealer.getLastName()).isEqualTo("Nowak");
        assertThat(carDealer.getEmail()).isEqualTo("nowak@gmail.com");
        assertThat(carDealer.getId()).isNotNull();
    }

    @Test
    @DisplayName("Should return status bad request")
    void shouldReturnStatusBadRequest() throws IOException, JSONException {
        //given
        final HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        final HttpEntity<String> request = new HttpEntity<>(readFile("carDealer/carDealerBadRequest.json"), headers);
        //when
        final ResponseEntity<String> result = restTemplate.postForEntity(
                "http://localhost:" + servicePort + "/carDealer",
                request,
                String.class
        );
        //then
        assertThat(result.getStatusCode()).isEqualTo(HttpStatus.BAD_REQUEST);
        JSONAssert.assertEquals(
                readFile("error/validationError.json"),
                result.getBody(),
                JSONCompareMode.NON_EXTENSIBLE
        );
    }

    @AfterEach
    void cleanup() {
        carDealerRepository.deleteAll();
    }
}

