package pl.tuchalski.kamil.carshop.carPurchase;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * Car purchase database management.
 */
@Repository
public interface CarPurchaseRepository extends JpaRepository<CarPurchase, Long> {
    CarPurchase findByCarId(Long id);
}
