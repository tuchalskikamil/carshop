package pl.tuchalski.kamil.carshop.carPurchase;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.List;

/**
 * Rest controller to manage purchase cars
 */
@RestController
public class CarPurchaseController {

    private static final Logger logger = LoggerFactory.getLogger(CarPurchaseController.class);

    private final CarPurchaseService carPurchaseService;

    /**
     * Instantiates CarPurchaseController
     *
     * @param carPurchaseService car purchase serice
     */
    public CarPurchaseController(final CarPurchaseService carPurchaseService) {
        this.carPurchaseService = carPurchaseService;
    }

    /**
     * Endpoint returning all cars purchases
     *
     * @return List of cars purchases
     */
    @GetMapping("/purchases")
    public List<CarPurchaseDto> getAllPurchases() {
        logger.info("GET/purchases   get all purchases request received");
        final List<CarPurchaseDto> allPurchase = carPurchaseService.getAllPurchase();
        logger.info("Returning cars purchases: " + allPurchase);
        return allPurchase;
    }

    /**
     * Endpoint adding new purchase
     *
     * @param carPurchaseDto new purchase
     * @return Status 204-NO CONTENT
     */
    @PostMapping("/purchases")
    public ResponseEntity<String> addPurchase(@Valid @RequestBody final CarPurchaseDto carPurchaseDto) {
        logger.info("POST/purchases  request received with body: " + carPurchaseDto);
        carPurchaseService.addCarPurchase(carPurchaseDto);
        logger.info("Returning NO CONTENT response");
        return ResponseEntity.noContent().build();
    }

}
