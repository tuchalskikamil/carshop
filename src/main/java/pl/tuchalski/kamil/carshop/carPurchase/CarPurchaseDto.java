package pl.tuchalski.kamil.carshop.carPurchase;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import java.time.Instant;
import java.util.Objects;

/**
 * Class representing car purchase DTO
 */
public class CarPurchaseDto {
    @NotNull
    private Long carId;
    @NotNull
    private Long clientId;
    @NotNull
    private Long carDealerId;
    @NotNull
    private Instant timestamp;
    @Min(0)
    private int price;
    private Long id;

    /**
     * Instantiates car purchase DTO
     */

    public CarPurchaseDto() {

    }

    /**
     * Instantiates car purchase DTO
     *
     * @param id        Car purchase DTO ID
     * @param timestamp Date for purchase car
     * @param price     price for purchase car
     */
    public CarPurchaseDto(
            final Long id,
            final Instant timestamp,
            final int price,
            final Long carId,
            final Long clientId,
            final Long carDealerId
    ) {
        this.id = id;
        this.timestamp = timestamp;
        this.price = price;
        this.carId = carId;
        this.clientId = clientId;
        this.carDealerId = carDealerId;
    }

    public Long getCarDealerId() {
        return carDealerId;
    }

    public void setCarDealerId(Long carDealerId) {
        this.carDealerId = carDealerId;
    }

    public Instant getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(final Instant timestamp) {
        this.timestamp = timestamp;
    }

    public Long getCarId() {
        return carId;
    }

    public void setCarId(final Long carId) {
        this.carId = carId;
    }

    public Long getClientId() {
        return clientId;
    }

    public void setClientId(final Long clientId) {
        this.clientId = clientId;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(final int price) {
        this.price = price;
    }

    public Long getId() {
        return id;
    }

    public void setId(final Long id) {
        this.id = id;
    }

    @Override
    public String toString() {
        return "CarPurchaseDto{" +
                "carId=" + carId +
                ", clientId=" + clientId +
                ", carDealerID=" + carDealerId +
                ", timestamp=" + timestamp +
                ", price=" + price +
                ", id=" + id +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        CarPurchaseDto that = (CarPurchaseDto) o;
        {
            return price == that.price
                    && Objects.equals(carId, that.carId)
                    && Objects.equals(clientId, that.clientId)
                    && Objects.equals(carDealerId, that.carDealerId)
                    && Objects.equals(timestamp, that.timestamp)
                    && Objects.equals(id, that.id);
        }
    }

    @Override
    public int hashCode() {
        return Objects.hash(carId, clientId, carDealerId, timestamp, price, id);
    }
}
