package pl.tuchalski.kamil.carshop.carPurchase;

import pl.tuchalski.kamil.carshop.car.Car;
import pl.tuchalski.kamil.carshop.client.Client;
import pl.tuchalski.kamil.carshop.carDealer.CarDealer;

import javax.persistence.*;
import java.time.Instant;
import java.util.Objects;

/**
 * Class representing purchase
 */
@Entity
public class CarPurchase {
    @OneToOne()
    @JoinColumn(name = "car_id", referencedColumnName = "id")
    private Car car;
    @ManyToOne
    @JoinColumn(name = "client_id")
    private Client client;
    @ManyToOne
    @JoinColumn(name = "user_id")
    private CarDealer carDealer;
    private Instant timestamp;
    private int price;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    /**
     * Instantiates Purchase
     */

    public CarPurchase() {

    }

    public CarDealer getCarDealer() {
        return carDealer;
    }

    public void setUser(CarDealer carDealer) {
        this.carDealer = carDealer;
    }

    public Car getCar() {
        return car;
    }

    public void setCar(final Car car) {
        this.car = car;
    }

    public Client getClient() {
        return client;
    }

    public void setClient(final Client client) {
        this.client = client;
    }

    public Instant getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(final Instant timestamp) {
        this.timestamp = timestamp;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(final int price) {
        this.price = price;
    }

    public Long getId() {
        return id;
    }

    public void setId(final Long id) {
        this.id = id;
    }

    @Override
    public String toString() {
        return "CarPurchase{" +
                "car=" + car +
                ", client=" + client +
                ", carDealer=" + carDealer +
                ", timestamp=" + timestamp +
                ", price=" + price +
                ", id=" + id +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        CarPurchase that = (CarPurchase) o;
        {
            return price == that.price
                    && Objects.equals(car, that.car)
                    && Objects.equals(client, that.client)
                    && Objects.equals(carDealer, that.carDealer)
                    && Objects.equals(timestamp, that.timestamp)
                    && Objects.equals(id, that.id);
        }
    }

    @Override
    public int hashCode() {
        return Objects.hash(car, client, carDealer, timestamp, price, id);
    }
}


