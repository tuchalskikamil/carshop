package pl.tuchalski.kamil.carshop.carPurchase;

import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;
import pl.tuchalski.kamil.carshop.car.Car;
import pl.tuchalski.kamil.carshop.car.CarRepository;

import java.util.List;

/**
 * Service to car purchase
 */
@Service
public class CarPurchaseService {
    private final CarPurchaseRepository carPurchaseRepository;
    private final ModelMapper modelMapper;
    private final CarRepository carRepository;

    /**
     * Instantiates CarPurchaseService
     *
     * @param carPurchaseRepository for database connection
     * @param modelMapper           for mapping CarPurchase and CarPurchaseDto
     * @param carRepository         connection with Car
     */
    public CarPurchaseService(final CarPurchaseRepository carPurchaseRepository,
                              final ModelMapper modelMapper,
                              final CarRepository carRepository) {
        this.carPurchaseRepository = carPurchaseRepository;
        this.modelMapper = modelMapper;
        this.carRepository = carRepository;
    }

    /**
     * Endpoint returning all cars purchases
     *
     * @return List of the cars purchases
     */
    public List<CarPurchaseDto> getAllPurchase() {
        return carPurchaseRepository.findAll().stream().map(this::convertToDto).toList();
    }

    /**
     * Endpoint adding new car purchase
     *
     * @param carPurchaseDto save new car purchase to database
     */
    public void addCarPurchase(final CarPurchaseDto carPurchaseDto) {
        final CarPurchase carPurchase = modelMapper.map(carPurchaseDto, CarPurchase.class);
        carPurchaseRepository.save(carPurchase);
        final Car car = carRepository.getById(carPurchaseDto.getCarId());
        car.setSold(true);
        carRepository.save(car);
    }

    private CarPurchaseDto convertToDto(final CarPurchase carPurchase) {
        return modelMapper.map(carPurchase, CarPurchaseDto.class);
    }

}
