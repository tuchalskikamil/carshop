package pl.tuchalski.kamil.carshop.car;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * Rest controller exposing endpoints with filtered cars list
 */
@RestController
public class CarFilterController {

    private static final Logger logger = LoggerFactory.getLogger(CarFilterController.class);

    private final CarFilterService carFilterService;

    /**
     * Instantiates CarFilterController
     *
     * @param carFilterService car service
     */
    public CarFilterController(final CarFilterService carFilterService) {
        this.carFilterService = carFilterService;
    }

    /**
     * Endpoint returning cars filtered by brand
     *
     * @param brand parameter filter
     * @return cars filtered by brand
     */
    @GetMapping(value = "/cars", params = "brand")
    public List<CarDto> getCarsFilteredByBrand(@RequestParam final String brand) {
        logger.info("GET /cars  get cars filtered by brand request received");
        final List<CarDto> carsFilteredByBrand = carFilterService.getCarsFilteredByBrand(brand);
        logger.info("Returning cars: " + carsFilteredByBrand);
        return carsFilteredByBrand;
    }

    /**
     * Endpoint returning cars filtered by sold
     *
     * @param sold parameter filter
     * @return cars filtered by sold parameter
     */
    @GetMapping(value = "/cars", params = "sold")
    public List<CarDto> getCarsFilteredBySold(@RequestParam final Boolean sold) {
        logger.info("GET/cars  get cars filtered by sold request received");
        final List<CarDto> carsFilteredBySold = carFilterService.getCarsFilteredBySold(sold);
        logger.info("Returning cars: " + carsFilteredBySold);
        return carsFilteredBySold;
    }

    /**
     * Endpoint returning cars filtered by production year
     *
     * @param minProductionYear parameter filter
     * @param maxProductionYear parameter filter
     * @return cars filtered by production year parameter
     */
    @GetMapping(value = "/cars", params = {"minProductionYear", "maxProductionYear"})
    public List<CarDto> getCarsFilteredByProductionYear(@RequestParam final int minProductionYear,
                                                        @RequestParam final int maxProductionYear) {
        logger.info("GET/cars  get cars filtered by productions year request received");
        final List<CarDto> carsFilteredByProductionsYear =
                carFilterService.getCarsFilteredByProductionYear(minProductionYear, maxProductionYear);
        logger.info("Returning cars: " + carsFilteredByProductionsYear);
        return carsFilteredByProductionsYear;
    }

    /**
     * Endpoint returning cars filtered by price
     *
     * @param minPrice parameter filter
     * @param maxPrice parameter filter
     * @return cars filtered by price parameter
     */
    @GetMapping(value = "/cars", params = {"minPrice", "maxPrice"})
    public List<CarDto> getCarsFilterByPrice(@RequestParam final Integer minPrice,
                                             @RequestParam final Integer maxPrice) {
        logger.info("GET/cars  get cars filtered by price request received");
        final List<CarDto> carsFilteredByPrice = carFilterService.getCarsFilteredByPrice(minPrice, maxPrice);
        logger.info("Returning cars: " + carsFilteredByPrice);
        return carsFilteredByPrice;
    }

}
