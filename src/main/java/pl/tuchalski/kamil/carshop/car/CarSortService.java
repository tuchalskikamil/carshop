package pl.tuchalski.kamil.carshop.car;

import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Service to receive sorted cars list
 */
@Service
public class CarSortService {
    private final CarRepository carRepository;
    private final CarToCarDtoMapper carToCarDtoMapper;

    /**
     * Instantiates CarSorService
     *
     * @param carRepository     for database connection
     * @param carToCarDtoMapper for mapping Car to CarDto
     */
    public CarSortService(final CarRepository carRepository, CarToCarDtoMapper carToCarDtoMapper) {
        this.carRepository = carRepository;
        this.carToCarDtoMapper = carToCarDtoMapper;
    }

    /**
     * Returning cars sorted by price
     *
     * @return cars sorted by price
     */
    public List<CarDto> getCarsSortByPrice() {
        return carToCarDtoMapper.mapCars(carRepository.findByOrderByPrice());
    }
}
