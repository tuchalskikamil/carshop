package pl.tuchalski.kamil.carshop.car;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.List;

/**
 * Rest controller to manage cars
 */
@RestController
public class CarController {

    private static final Logger logger = LoggerFactory.getLogger(CarController.class);

    private final CarService carService;

    /**
     * Instantiates CarController
     *
     * @param carService car service
     */
    public CarController(final CarService carService) {
        this.carService = carService;
    }

    /**
     * Endpoint returning all cars
     *
     * @return All cars
     */
    @GetMapping("/cars")
    public List<CarDto> getAllCars() {
        logger.info("GET /cars   get all cars request received");
        final List<CarDto> allCars = carService.getAllCars();
        logger.info("Returning cars: " + allCars);
        return allCars;
    }


    /**
     * Endpoint adding new car
     *
     * @param carDto save new car to database DTO
     * @return Status 204-NO CONTENT
     */
    @PostMapping("/cars")
    public ResponseEntity<String> addCar(@Valid @RequestBody final CarSaveDto carDto) {
        logger.info("POST /cars  request received with body: " + carDto);
        carService.addCar(carDto);
        logger.info("Returning NO CONTENT response");
        return ResponseEntity.noContent().build();
    }
}
