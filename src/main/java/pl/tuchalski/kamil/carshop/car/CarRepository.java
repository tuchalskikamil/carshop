package pl.tuchalski.kamil.carshop.car;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Car database management.
 */
@Repository
public interface CarRepository extends JpaRepository<Car, Long> {
    List<Car> findByBrand(String brand);

    List<Car> findBySold(Boolean sold);

    List<Car> findByProductionYearBetween(Integer minProductionYear, Integer maxProductionYear);

    List<Car> findByPriceBetween(int minPrice, int maxPrice);

    List<Car> findByOrderByPrice();
}
