package pl.tuchalski.kamil.carshop.car.audi;

import java.util.Objects;

public class CarAudi {
    private String brand;
    private String model;
    private String colour;
    private String numberOfDoors;
    private String bodyType;
    private long id;

    public CarAudi() {

    }

    public CarAudi(
            final String brand,
            final String model,
            final String colour,
            final String numberOfDoors,
            final String bodyType,
            final long id
    ) {
        this.brand = brand;
        this.model = model;
        this.colour = colour;
        this.numberOfDoors = numberOfDoors;
        this.bodyType = bodyType;
        this.id = id;
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public String getColour() {
        return colour;
    }

    public void setColour(String colour) {
        this.colour = colour;
    }

    public String getNumberOfDoors() {
        return numberOfDoors;
    }

    public void setNumberOfDoors(String numberOfDoors) {
        this.numberOfDoors = numberOfDoors;
    }

    public String getBodyType() {
        return bodyType;
    }

    public void setBodyType(String bodyType) {
        this.bodyType = bodyType;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    @Override
    public String toString() {
        return "CarAudi{" +
                "brand='" + brand + '\'' +
                ", model='" + model + '\'' +
                ", colour='" + colour + '\'' +
                ", numberOfDoors=" + numberOfDoors +
                ", bodyType='" + bodyType + '\'' +
                ", id=" + id +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        CarAudi carAudi = (CarAudi) o;
        {
            return Objects.equals(numberOfDoors, carAudi.numberOfDoors)
                    && id == carAudi.id
                    && Objects.equals(brand, carAudi.brand)
                    && Objects.equals(model, carAudi.model)
                    && Objects.equals(colour, carAudi.colour)
                    && Objects.equals(bodyType, carAudi.bodyType);
        }
    }

    @Override
    public int hashCode() {
        return Objects.hash(brand, model, colour, numberOfDoors, bodyType, id);
    }
}
