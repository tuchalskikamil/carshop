package pl.tuchalski.kamil.carshop.car;

import org.modelmapper.Converter;
import org.modelmapper.spi.MappingContext;
import org.springframework.stereotype.Component;

/**
 * Converter class
 */
@Component
public class CarIdToCarConverter implements Converter<Long, Car> {

    /**
     * Converts car id to car
     *
     * @param context contains values to convert
     * @return new car
     */
    @Override
    public Car convert(MappingContext<Long, Car> context) {
        return createCar(context.getSource());
    }

    private Car createCar(final Long id) {
        final Car car = new Car();
        car.setId(id);
        return car;
    }
}
