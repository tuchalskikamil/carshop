package pl.tuchalski.kamil.carshop.car;

import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Service filters to cars
 */
@Service
public class CarFilterService {
    private final CarRepository carRepository;
    private final CarToCarDtoMapper carToCarDtoMapper;

    /**
     * Instantiates CarFilterService
     *
     * @param carRepository     for database connection
     * @param carToCarDtoMapper for mapping Car to CarDto
     */
    public CarFilterService(final CarRepository carRepository,
                            final CarToCarDtoMapper carToCarDtoMapper) {
        this.carRepository = carRepository;
        this.carToCarDtoMapper = carToCarDtoMapper;
    }

    /**
     * Returning cars filtered by brand
     *
     * @param brand parameter filter
     * @return cars filtered by brand
     */
    public List<CarDto> getCarsFilteredByBrand(final String brand) {
        return carToCarDtoMapper.mapCars(carRepository.findByBrand(brand));
    }

    /**
     * Returning cars filtered by sold
     *
     * @param sold parameter filter
     * @return cars filtered by sold
     */
    public List<CarDto> getCarsFilteredBySold(final Boolean sold) {
        return carToCarDtoMapper.mapCars(carRepository.findBySold(sold));
    }

    /**
     * Returning cars filtered by production year
     *
     * @param minProductionYear parameter filter
     * @param maxProductionYear parameter filter
     * @return cars filtered by production year parameter
     */
    public List<CarDto> getCarsFilteredByProductionYear(final Integer minProductionYear,
                                                        final Integer maxProductionYear) {
        return carToCarDtoMapper.mapCars(carRepository.findByProductionYearBetween(minProductionYear,
                maxProductionYear));
    }

    /**
     * Returning cars filtered by price
     *
     * @param minPrice parameter filter
     * @param maxPrice parameter filter
     * @return cars filtered by price
     */
    public List<CarDto> getCarsFilteredByPrice(final int minPrice, final int maxPrice) {
        return carToCarDtoMapper.mapCars(carRepository.findByPriceBetween(minPrice, maxPrice));
    }
}
