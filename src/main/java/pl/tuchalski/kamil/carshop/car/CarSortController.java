package pl.tuchalski.kamil.carshop.car;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * Rest controller with cars sorting
 */
@RestController
public class CarSortController {
    private static final Logger logger = LoggerFactory.getLogger(CarSortController.class);
    private final CarSortService carSortService;

    /**
     * Instantiates CarSortController
     *
     * @param carSortService car service
     */
    public CarSortController(final CarSortService carSortService) {
        this.carSortService = carSortService;
    }

    @GetMapping(value = "/cars", params = "sort")
    public List<CarDto> getCarsSortByPrice() {
        logger.info("GET /cars  get cars sort by price request received");
        final List<CarDto> carsSortByPrice = carSortService.getCarsSortByPrice();
        logger.info("Returning cars: " + carsSortByPrice);
        return carsSortByPrice;
    }

}
