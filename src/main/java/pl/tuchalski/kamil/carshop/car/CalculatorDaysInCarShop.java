package pl.tuchalski.kamil.carshop.car;

import org.springframework.stereotype.Component;

import java.time.Instant;
import java.util.concurrent.TimeUnit;

/**
 * Calculator days in car shop
 */
@Component
public class CalculatorDaysInCarShop {

    /**
     * Calculator days in car shop
     *
     * @param carDto gets the date the car was placed in the car shop
     * @param toDate gets the date of purchases
     * @return how many days is the car in the car shop
     */
    public long daysInCarShopToDate(final CarDto carDto, final Instant toDate) {
        final long secondsInCarsShop = toDate.getEpochSecond() - carDto.getTimestamp().getEpochSecond();
        return TimeUnit.SECONDS.toDays(secondsInCarsShop);
    }
}
