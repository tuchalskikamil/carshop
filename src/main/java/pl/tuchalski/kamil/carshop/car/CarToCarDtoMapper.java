package pl.tuchalski.kamil.carshop.car;

import org.modelmapper.ModelMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import pl.tuchalski.kamil.carshop.car.audi.AudiInformationService;
import pl.tuchalski.kamil.carshop.car.audi.CarAudi;
import pl.tuchalski.kamil.carshop.carPurchase.CarPurchase;
import pl.tuchalski.kamil.carshop.carPurchase.CarPurchaseRepository;

import java.time.Instant;
import java.util.List;

/**
 * Mapper Car to Car DTO
 */
@Component
public class CarToCarDtoMapper {
    private static final Logger logger = LoggerFactory.getLogger(CarToCarDtoMapper.class);
    private final ModelMapper modelMapper;
    private final CalculatorDaysInCarShop calculatorDaysInCarShop;
    private final CarPurchaseRepository carPurchaseRepository;
    private final AudiInformationService audiInformationService;

    /**
     * Instantiates CarToCarDtoMapper
     *
     * @param modelMapper             for mapping Car and CarDto
     * @param calculatorDaysInCarShop calculation days in car shop
     * @param carPurchaseRepository   for database connection
     * @param audiInformationService  connection with Audi server
     */
    public CarToCarDtoMapper(final ModelMapper modelMapper,
                             final CalculatorDaysInCarShop calculatorDaysInCarShop,
                             final CarPurchaseRepository carPurchaseRepository,
                             final AudiInformationService audiInformationService) {
        this.modelMapper = modelMapper;
        this.calculatorDaysInCarShop = calculatorDaysInCarShop;
        this.carPurchaseRepository = carPurchaseRepository;
        this.audiInformationService = audiInformationService;
    }

    /**
     * Mapper Car to CarDto
     * Added calculator days in car shop
     *
     * @param cars cars in Car Shop
     * @return List car DTO
     */
    public List<CarDto> mapCars(final List<Car> cars) {
        final List<CarDto> carDtos = cars.stream().map(this::convertToDto).toList();
        addDaysInCarShop(carDtos);
        addAudiData(carDtos);
        return carDtos;
    }

    private void addAudiData(final List<CarDto> carDtos) {
        for (CarDto carDto : carDtos) {
            if (carDto.getBrand().equals("Audi")) {
                try {
                    addAudiDataFor(carDto);
                } catch (Exception exception) {
                    logger.error("No Audi model found in the service", exception);
                }
            }
        }
    }

    private void addAudiDataFor(final CarDto carDto) {
        final CarAudi carAudi = audiInformationService.fetchAudiInformation(carDto.getModel());
        carDto.setColour(carAudi.getColour());
        carDto.setNumberOfDoors(carAudi.getNumberOfDoors());
        carDto.setBodyType(carAudi.getBodyType());
    }

    private CarDto convertToDto(final Car car) {
        return modelMapper.map(car, CarDto.class);
    }

    private long countDaysInCarShop(final CarDto carDto) {
        if (carDto.getSold()) {
            final CarPurchase carPurchase = carPurchaseRepository.findByCarId(carDto.getId());
            return calculatorDaysInCarShop.daysInCarShopToDate(carDto, carPurchase.getTimestamp());
        } else {
            return calculatorDaysInCarShop.daysInCarShopToDate(carDto, Instant.now());
        }
    }

    private void addDaysInCarShop(final List<CarDto> carDtos) {
        for (CarDto carDto : carDtos) {
            carDto.setDaysInCarsShop(countDaysInCarShop(carDto));
        }
    }

}
