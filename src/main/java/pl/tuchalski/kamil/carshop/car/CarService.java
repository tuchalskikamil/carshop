package pl.tuchalski.kamil.carshop.car;

import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Service to cars
 */
@Service
public class CarService {
    private final CarRepository carRepository;
    private final ModelMapper modelMapper;
    private final CarToCarDtoMapper carToCarDtoMapper;

    /**
     * Instantiates CarService
     *
     * @param carRepository     for database connection
     * @param modelMapper       for mapping Car and CarDto
     * @param carToCarDtoMapper for mapper Car to CarDto
     */
    public CarService(final CarRepository carRepository,
                      final ModelMapper modelMapper,
                      final CarToCarDtoMapper carToCarDtoMapper) {
        this.carRepository = carRepository;
        this.modelMapper = modelMapper;
        this.carToCarDtoMapper = carToCarDtoMapper;
    }

    /**
     * Returning all cars
     *
     * @return List of the cars
     */
    public List<CarDto> getAllCars() {
        return carToCarDtoMapper.mapCars(carRepository.findAll());
    }

    /**
     * Endpoint adding new car
     *
     * @param carDto save new car to database
     */
    public void addCar(final CarSaveDto carDto) {
        final Car car = modelMapper.map(carDto, Car.class);
        carRepository.save(car);
    }
}
