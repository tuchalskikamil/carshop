package pl.tuchalski.kamil.carshop.car.audi;

import org.springframework.stereotype.Component;
import org.springframework.web.reactive.function.client.WebClient;

@Component
public class AudiInformationService {
    private final WebClient webClient;

    public AudiInformationService(final WebClient webClient) {
        this.webClient = webClient;
    }

    public CarAudi fetchAudiInformation(String model) {
        return webClient
                .get()
                .uri(uriBuilder -> uriBuilder
                        .path("/audi/{model}/information")
                        .build(model))
                .retrieve()
                .toEntity(CarAudi.class)
                .block()
                .getBody();
    }

}
