package pl.tuchalski.kamil.carshop.car;

import com.fasterxml.jackson.annotation.JsonInclude;

import java.time.Instant;
import java.util.Objects;

/**
 * Class representing car DTO
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
public class CarDto extends CarSaveDto {
    private Long id;
    private Boolean sold;
    private long daysInCarsShop;
    private String colour;
    private String numberOfDoors;
    private String bodyType;

    /**
     * Instantiates Car
     */
    public CarDto() {

    }

    /**
     * @param brand          brand of the car
     * @param model          model of the car
     * @param productionYear productions year of the car
     * @param price          price of the car
     * @param id             ID fo car
     * @param sold           status of car
     * @param timestamp      date the car was added
     * @param daysInCarsShop information on how many days the car is in car shop
     * @param colour         information color retrieved from an external server
     * @param numberOfDoors  information number of doors retrieved from an external server
     * @param bodyType       information body type retrieved from an external server
     */
    public CarDto(final String brand,
                  final String model,
                  final int productionYear,
                  final int price,
                  final Instant timestamp,
                  final Long id,
                  final Boolean sold,
                  final long daysInCarsShop,
                  final String colour,
                  final String numberOfDoors,
                  final String bodyType
    ) {
        super(brand, model, productionYear, price, timestamp);
        this.id = id;
        this.sold = sold;
        this.daysInCarsShop = daysInCarsShop;
        this.colour = colour;
        this.numberOfDoors = numberOfDoors;
        this.bodyType = bodyType;
    }

    public String getNumberOfDoors() {
        return numberOfDoors;
    }

    public void setNumberOfDoors(String numberOfDoors) {
        this.numberOfDoors = numberOfDoors;
    }

    public String getBodyType() {
        return bodyType;
    }

    public void setBodyType(String bodyType) {
        this.bodyType = bodyType;
    }

    public String getColour() {
        return colour;
    }

    public void setColour(String colour) {
        this.colour = colour;
    }

    public long getDaysInCarsShop() {
        return daysInCarsShop;
    }

    public void setDaysInCarsShop(final long daysInCarsShop) {
        this.daysInCarsShop = daysInCarsShop;
    }

    public Boolean getSold() {
        return sold;
    }

    public void setSold(final Boolean sold) {
        this.sold = sold;
    }

    public Long getId() {
        return id;
    }

    public void setId(final Long id) {
        this.id = id;
    }

    @Override
    public String toString() {
        return "CarDto{" +
                "id=" + id +
                ", sold=" + sold +
                ", daysInCarsShop=" + daysInCarsShop +
                ", colour='" + colour + '\'' +
                ", numberOfDoors='" + numberOfDoors + '\'' +
                ", bodyType='" + bodyType + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        if (!super.equals(o)) {
            return false;
        }
        CarDto carDto = (CarDto) o;
        {
            return daysInCarsShop == carDto.daysInCarsShop
                    && Objects.equals(id, carDto.id)
                    && Objects.equals(sold,
                    carDto.sold)
                    && Objects.equals(colour, carDto.colour)
                    && Objects.equals(numberOfDoors,
                    carDto.numberOfDoors)
                    && Objects.equals(bodyType, carDto.bodyType);
        }
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), id, sold, daysInCarsShop, colour, numberOfDoors, bodyType);
    }

}