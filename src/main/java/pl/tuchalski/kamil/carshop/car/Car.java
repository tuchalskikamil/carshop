package pl.tuchalski.kamil.carshop.car;

import pl.tuchalski.kamil.carshop.carPurchase.CarPurchase;

import javax.persistence.*;
import java.time.Instant;
import java.util.Objects;

/**
 * Class representing car
 */
@Entity
public class Car {
    private String brand;
    private String model;
    private int productionYear;
    private int price;
    @OneToOne(mappedBy = "car")
    private CarPurchase carPurchase;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private Boolean sold;
    private Instant timestamp;


    /**
     * Instantiates Car
     */
    public Car() {

    }

    /**
     * Instantiates Car
     *
     * @param brand          brand of the car
     * @param model          model of the car
     * @param productionYear productions year of the car
     * @param price          price of the car
     * @param sold           status of car
     * @param timestamp      date the car was added
     */
    public Car(final String brand,
               final String model,
               final int productionYear,
               final int price,
               final Boolean sold,
               final Instant timestamp) {
        this.brand = brand;
        this.model = model;
        this.productionYear = productionYear;
        this.price = price;
        this.sold = sold;
        this.timestamp = timestamp;
    }

    public Instant getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(final Instant timestamp) {
        this.timestamp = timestamp;
    }

    public Boolean getSold() {
        return sold;
    }

    public void setSold(final Boolean sold) {
        this.sold = sold;
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(final String brand) {
        this.brand = brand;
    }

    public String getModel() {
        return model;
    }

    public void setModel(final String model) {
        this.model = model;
    }

    public int getProductionYear() {
        return productionYear;
    }

    public void setProductionYear(final int productionYear) {
        this.productionYear = productionYear;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(final int price) {
        this.price = price;
    }

    public Long getId() {
        return id;
    }

    public void setId(final Long id) {
        this.id = id;
    }

    public CarPurchase getCarPurchase() {
        return carPurchase;
    }

    public void setCarPurchase(final CarPurchase carPurchase) {
        this.carPurchase = carPurchase;
    }

    @Override
    public String toString() {
        return "Car{" +
                "brand='" + brand + '\'' +
                ", model='" + model + '\'' +
                ", productionYear=" + productionYear +
                ", price=" + price +
                ", carPurchase=" + carPurchase +
                ", id=" + id +
                ", sold=" + sold +
                ", timestamp=" + timestamp +
                '}';
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Car car = (Car) o;
        return productionYear == car.productionYear
                && price == car.price
                && Objects.equals(brand, car.brand)
                && Objects.equals(model, car.model)
                && Objects.equals(carPurchase, car.carPurchase)
                && Objects.equals(id, car.id)
                && Objects.equals(sold, car.sold)
                && Objects.equals(timestamp, car.timestamp);
    }

    @Override
    public int hashCode() {
        return Objects.hash(brand, model, productionYear, price, carPurchase, id, sold, timestamp);
    }
}
