package pl.tuchalski.kamil.carshop.car;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.time.Instant;
import java.util.Objects;

/**
 * Class representing car DTO
 */
public class CarSaveDto {
    @NotBlank(message = "Brand is mandatory and should not be empty")
    private String brand;
    @NotBlank(message = "Model is mandatory and should not be empty")
    private String model;
    @Min(1900)
    @Max(2050)
    private int productionYear;
    @Min(0)
    private int price;
    @NotNull
    private Instant timestamp;


    /**
     * Instantiates Car
     */
    public CarSaveDto() {

    }

    /**
     * Instantiates Car DTO
     *
     * @param brand          brand of the car
     * @param model          model of the car
     * @param productionYear productions year of the car
     * @param price          price of the car
     * @param timestamp      date the car was added
     */
    public CarSaveDto(final String brand,
                      final String model,
                      final int productionYear,
                      final int price,
                      final Instant timestamp) {
        this.brand = brand;
        this.model = model;
        this.productionYear = productionYear;
        this.price = price;
        this.timestamp = timestamp;
    }

    public Instant getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(final Instant timestamp) {
        this.timestamp = timestamp;
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(final String brand) {
        this.brand = brand;
    }

    public String getModel() {
        return model;
    }

    public void setModel(final String model) {
        this.model = model;
    }

    public int getProductionYear() {
        return productionYear;
    }

    public void setProductionYear(final int productionYear) {
        this.productionYear = productionYear;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(final int price) {
        this.price = price;
    }

    @Override
    public String toString() {
        return "CarSaveDto{" +
                "brand='" + brand + '\'' +
                ", model='" + model + '\'' +
                ", productionYear=" + productionYear +
                ", price=" + price +
                ", timestamp=" + timestamp +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        CarSaveDto that = (CarSaveDto) o;
        return productionYear == that.productionYear
                && price == that.price
                && Objects.equals(brand, that.brand)
                && Objects.equals(model, that.model)
                && Objects.equals(timestamp, that.timestamp);
    }

    @Override
    public int hashCode() {
        return Objects.hash(brand, model, productionYear, price, timestamp);
    }
}
