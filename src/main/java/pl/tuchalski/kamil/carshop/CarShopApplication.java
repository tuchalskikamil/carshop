package pl.tuchalski.kamil.carshop;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * Main class
 */
@SpringBootApplication
public class CarShopApplication {

    /**
     * Runs Spring Boot app
     *
     * @param args arguments
     */
    public static void main(final String[] args) {
        SpringApplication.run(CarShopApplication.class, args);
    }
}
