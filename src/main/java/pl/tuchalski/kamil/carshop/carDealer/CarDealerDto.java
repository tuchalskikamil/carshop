package pl.tuchalski.kamil.carshop.carDealer;


import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;
import java.util.Objects;

/**
 * Class representing car dealer DTO
 */
public class CarDealerDto {
    @NotBlank(message = "First Name is mandatory and should not be empty")
    private String firstName;
    @NotBlank(message = "Last Name is mandatory and should not be empty")
    private String lastName;
    @Pattern(regexp = "^(?=.{1,64}@)[A-Za-z0-9_-]+(\\.[A-Za-z0-9_-]+)*@"
            + "[^-][A-Za-z0-9-]+(\\.[A-Za-z0-9-]+)*(\\.[A-Za-z]{2,})$", message = "cars dealer's e-mail, must by e.g. " +
            "username@domain.com")
    private String email;
    private Long id;

    /**
     * Instantiates Car dealer DTO
     */
    public CarDealerDto() {

    }

    /**
     * Instantiates car dealer DTO
     *
     * @param firstName Name of the car dealer
     * @param lastName  Last Name of the car dealer
     * @param email     Email of the car dealer
     * @param id        car dealer DTO ID
     */
    public CarDealerDto(final String firstName, final String lastName, final String email, final Long id) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.email = email;
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lasName) {
        this.lastName = lasName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Override
    public String toString() {
        return "CarDealerDto{" +
                "firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", email='" + email + '\'' +
                ", id=" + id +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        CarDealerDto carDealerDto = (CarDealerDto) o;
        {
            return Objects.equals(firstName, carDealerDto.firstName)
                    && Objects.equals(lastName, carDealerDto.lastName)
                    && Objects.equals(email, carDealerDto.email)
                    && Objects.equals(id, carDealerDto.id);
        }
    }

    @Override
    public int hashCode() {
        return Objects.hash(firstName, lastName, email, id);
    }
}
