package pl.tuchalski.kamil.carshop.carDealer;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * Car dealer database management
 */
@Repository
public interface CarDealerRepository extends JpaRepository<CarDealer, Long> {
}
