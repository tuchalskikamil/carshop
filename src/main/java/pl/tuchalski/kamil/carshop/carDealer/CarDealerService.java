package pl.tuchalski.kamil.carshop.carDealer;


import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

/**
 * Service to car dealer
 */
@Service
public class CarDealerService {
    private final CarDealerRepository carDealerRepository;
    private final ModelMapper modelMapper;

    /**
     * Instantiates car dealer service
     *
     * @param carDealerRepository for database connection
     * @param modelMapper         for mapping CarDealer and CarDealerDto
     */
    public CarDealerService(final CarDealerRepository carDealerRepository, final ModelMapper modelMapper) {
        this.carDealerRepository = carDealerRepository;
        this.modelMapper = modelMapper;
    }

    /**
     * Method returning all car dealers
     *
     * @return List of the car dealers
     */
    public List<CarDealerDto> getAllCarDealer() {
        return carDealerRepository.findAll().stream().map(this::convertToDto).collect(Collectors.toList());
    }

    /**
     * Method adding new car dealer
     *
     * @param carDealerDto save to new car dealer
     */
    public void addCarDealer(final CarDealerDto carDealerDto) {
        final CarDealer carDealer = modelMapper.map(carDealerDto, CarDealer.class);
        carDealerRepository.save(carDealer);
    }

    private CarDealerDto convertToDto(final CarDealer carDealer) {
        return modelMapper.map(carDealer, CarDealerDto.class);
    }
}
