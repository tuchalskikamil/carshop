package pl.tuchalski.kamil.carshop.carDealer;

import org.modelmapper.Converter;
import org.modelmapper.spi.MappingContext;
import org.springframework.stereotype.Component;

/**
 * Converter class
 */
@Component
public class CarDealerIdToCarDealerConverter implements Converter<Long, CarDealer> {

    /**
     * Converts car dealer id to car dealer
     *
     * @param context contains values to convert
     * @return new car dealer
     */
    @Override
    public CarDealer convert(final MappingContext<Long, CarDealer> context) {
        return createCarDealer(context.getSource());
    }

    private CarDealer createCarDealer(final Long id) {
        final CarDealer carDealer = new CarDealer();
        carDealer.setId(id);
        return carDealer;
    }
}
