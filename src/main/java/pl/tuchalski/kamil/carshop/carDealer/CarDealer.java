package pl.tuchalski.kamil.carshop.carDealer;

import pl.tuchalski.kamil.carshop.carPurchase.CarPurchase;

import javax.persistence.*;
import java.util.List;
import java.util.Objects;

/**
 * Class representing car dealer
 */
@Entity
public class CarDealer {
    private String firstName;
    private String lastName;
    private String email;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @OneToMany(mappedBy = "carDealer", fetch = FetchType.EAGER)
    private List<CarPurchase> carPurchases;

    /**
     * Instantiates car dealer
     */
    public CarDealer() {

    }

    /**
     * Instantiates car dealer
     *
     * @param firstName Name of the car dealer
     * @param lastName  Last name of the car dealer
     * @param email     Individual email of car dealer
     */
    public CarDealer(final String firstName, final String lastName, final String email) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.email = email;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public List<CarPurchase> getCarPurchases() {
        return carPurchases;
    }

    public void setCarPurchases(List<CarPurchase> carPurchases) {
        this.carPurchases = carPurchases;
    }

    @Override
    public String toString() {
        return "CarDealer{" +
                "firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", email='" + email + '\'' +
                ", id=" + id +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        CarDealer carDealer = (CarDealer) o;
        {
            return Objects.equals(firstName, carDealer.firstName)
                    && Objects.equals(lastName, carDealer.lastName)
                    && Objects.equals(email, carDealer.email)
                    && Objects.equals(id, carDealer.id)
                    && Objects.equals(carPurchases, carDealer.carPurchases);
        }
    }

    @Override
    public int hashCode() {
        return Objects.hash(firstName, lastName, email, id, carPurchases);
    }
}
