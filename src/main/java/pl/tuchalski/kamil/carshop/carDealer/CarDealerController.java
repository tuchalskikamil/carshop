package pl.tuchalski.kamil.carshop.carDealer;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.List;

/**
 * Rest controller to manage car dealer
 */
@RestController
public class CarDealerController {
    private static final Logger logger = LoggerFactory.getLogger(CarDealerController.class);
    private final CarDealerService carDealerService;

    /**
     * Instantiates car dealer controller
     *
     * @param carDealerService car dealer service
     */
    public CarDealerController(final CarDealerService carDealerService) {
        this.carDealerService = carDealerService;
    }

    /**
     * Endpoint returning all cars dealers
     *
     * @return List of the car dealers
     */
    @GetMapping("/carDealer")
    public List<CarDealerDto> getAllCarDealer() {
        logger.info("GET/car dealers   get all cars dealers request received");
        final List<CarDealerDto> allCarDealer = carDealerService.getAllCarDealer();
        logger.info("Returning car dealers: " + allCarDealer);
        return allCarDealer;
    }

    /**
     * Endpoint adding new car dealer
     *
     * @param carDealerDto save new car dealer to database
     * @return status 204-NO CONTENT
     */
    @PostMapping("/carDealer")
    public ResponseEntity<String> addCarDealer(@Valid @RequestBody final CarDealerDto carDealerDto) {
        logger.info("POST /car dealer  request received with body: " + carDealerDto);
        carDealerService.addCarDealer(carDealerDto);
        logger.info("Returning NO CONTENT response");
        return ResponseEntity.noContent().build();
    }
}
