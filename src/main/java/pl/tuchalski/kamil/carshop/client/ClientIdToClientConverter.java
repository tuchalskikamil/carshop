package pl.tuchalski.kamil.carshop.client;

import org.modelmapper.Converter;
import org.modelmapper.spi.MappingContext;
import org.springframework.stereotype.Component;

/**
 * Converter class
 */
@Component
public class ClientIdToClientConverter implements Converter<Long, Client> {

    /**
     * Converts client id to client
     *
     * @param context contains values to convert
     * @return new client
     */
    @Override
    public Client convert(MappingContext<Long, Client> context) {
        return createClient(context.getSource());
    }

    private Client createClient(final Long id) {
        final Client client = new Client();
        client.setId(id);
        return client;
    }
}
