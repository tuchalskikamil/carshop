package pl.tuchalski.kamil.carshop.client;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;
import java.util.Objects;

/**
 * Class representing client DTO
 */

public class ClientDto {
    @NotBlank(message = "First Name is mandatory and should not be empty")
    private String firstName;
    @NotBlank(message = "Last Name is mandatory and should not be empty")
    private String lastName;
    @Pattern(regexp = "[\\d]{9}", message = "Invalid phone number, must be e.g. 123123123")
    private String phoneNumber;
    private Long id;

    /**
     * Instantiates Client DTO
     */
    public ClientDto() {

    }

    /**
     * Instantiates Client DTO
     *
     * @param id          Client DTO ID
     * @param firstName   Name of the client
     * @param lastName    Last name of the client
     * @param phoneNumber Phone number of the client
     */
    public ClientDto(final Long id, final String firstName, final String lastName, final String phoneNumber) {
        this.id = id;
        this.firstName = firstName;
        this.lastName = lastName;
        this.phoneNumber = phoneNumber;
    }

    public Long getId() {
        return id;
    }

    public void setId(final Long id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(final String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(final String lastName) {
        this.lastName = lastName;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(final String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    @Override
    public String toString() {
        return "ClientDto{" +
                "firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", phoneNumber='" + phoneNumber + '\'' +
                ", id=" + id +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        ClientDto clientDto = (ClientDto) o;
        return Objects.equals(firstName, clientDto.firstName) &&
                Objects.equals(lastName, clientDto.lastName) &&
                Objects.equals(phoneNumber, clientDto.phoneNumber) &&
                Objects.equals(id, clientDto.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(firstName, lastName, phoneNumber, id);
    }
}
