package pl.tuchalski.kamil.carshop.client;

import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

/**
 * Service to client
 */
@Service
public class ClientService {
    private final ClientRepository clientRepository;
    private final ModelMapper modelMapper;

    /**
     * Instantiates ClientService
     *
     * @param clientRepository for database connection
     * @param modelMapper      for mapping Client and ClientDto
     */
    public ClientService(final ClientRepository clientRepository, final ModelMapper modelMapper) {
        this.clientRepository = clientRepository;
        this.modelMapper = modelMapper;
    }

    /**
     * Endpoint returning all clients
     *
     * @return List of the clients
     */
    public List<ClientDto> getAllClients() {
        return clientRepository.findAll().stream().map(this::convertToDto).collect(Collectors.toList());
    }

    /**
     * Endpoint adding new client
     *
     * @param clientDto save to new client
     */
    public void addClient(final ClientDto clientDto) {
        final Client client = modelMapper.map(clientDto, Client.class);
        clientRepository.save(client);
    }

    private ClientDto convertToDto(final Client client) {
        return modelMapper.map(client, ClientDto.class);
    }
}
