package pl.tuchalski.kamil.carshop.client;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.List;

/**
 * Rest controller to manage clients
 */
@RestController
public class ClientController {

    private static final Logger logger = LoggerFactory.getLogger(ClientController.class);

    private final ClientService clientService;

    private final String wordClient;


    /**
     * Instantiates ClientController
     *
     * @param clientService client service
     */
    public ClientController(final ClientService clientService,
                            @Value("${word.clients}") final String wordClient) {
        this.clientService = clientService;
        this.wordClient = wordClient;
    }


    /**
     * Endpoint returning all clients
     *
     * @return List of the clients
     */
    @GetMapping("/clients")
    public List<ClientDto> getAllClients() {
        logger.info("GET/" + wordClient + "   get all clients request received");
        final List<ClientDto> allClients = clientService.getAllClients();
        logger.info("Returning clients: " + allClients);
        return allClients;
    }

    /**
     * Endpoint adding new client
     *
     * @param clientDto save new client to database
     * @return status 204-NO CONTENT
     */
    @PostMapping("/clients")
    public ResponseEntity<String> addClient(@Valid @RequestBody final ClientDto clientDto) {
        logger.info("POST /clients  request received with body: " + clientDto);
        clientService.addClient(clientDto);
        logger.info("Returning NO CONTENT response");
        return ResponseEntity.noContent().build();
    }
}
