package pl.tuchalski.kamil.carshop.client;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * Client database management
 */
@Repository
public interface ClientRepository extends JpaRepository<Client, Long> {
}
