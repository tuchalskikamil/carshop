package pl.tuchalski.kamil.carshop.client;

import pl.tuchalski.kamil.carshop.carPurchase.CarPurchase;

import javax.persistence.*;
import java.util.List;
import java.util.Objects;

/**
 * Class representing client
 */
@Entity
public class Client {
    private String firstName;
    private String lastName;
    private String phoneNumber;
    @OneToMany(mappedBy = "client", fetch = FetchType.EAGER)
    private List<CarPurchase> carPurchases;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    /**
     * Instantiates Client
     */
    public Client() {

    }

    /**
     * Instantiates Client
     *
     * @param firstName   Name of the client
     * @param lastName    Last name of the client
     * @param phoneNumber Phone number of the client
     */
    public Client(final String firstName, final String lastName, final String phoneNumber) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.phoneNumber = phoneNumber;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(final String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(final String lastName) {
        this.lastName = lastName;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(final String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public Long getId() {
        return id;
    }

    public void setId(final Long id) {
        this.id = id;
    }

    public List<CarPurchase> getCarPurchases() {
        return carPurchases;
    }

    public void setCarPurchases(final List<CarPurchase> carPurchases) {
        this.carPurchases = carPurchases;
    }

    @Override
    public String toString() {
        return "Client{" +
                "firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", phoneNumber='" + phoneNumber + '\'' +
                ", id=" + id +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Client client = (Client) o;
        return Objects.equals(firstName, client.firstName) &&
                Objects.equals(lastName, client.lastName) &&
                Objects.equals(phoneNumber, client.phoneNumber) &&
                Objects.equals(carPurchases, client.carPurchases) &&
                Objects.equals(id, client.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(firstName, lastName, phoneNumber, carPurchases, id);
    }
}

