package pl.tuchalski.kamil.carshop.errors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

/**
 * A class that catches service errors and converts them into a response
 */
@RestControllerAdvice
public class ErrorHandler {
    private static final Logger logger = LoggerFactory.getLogger(ErrorHandler.class);

    /**
     * Method that catches the validation error
     *
     * @param exception this is validation exception
     * @return information about the validation error and HTTP status bad request
     */
    @ExceptionHandler(MethodArgumentNotValidException.class)
    public ResponseEntity<ErrorMessage> validationErrorHandler(final MethodArgumentNotValidException exception) {
        logger.error("Validation error", exception);
        return new ResponseEntity<>(
                new ErrorMessage("Validation Error"),
                HttpStatus.BAD_REQUEST
        );
    }

    /**
     * Method that catches exception
     *
     * @param exception this is server error
     * @return information about the server error and HTTP status internal server error
     */
    @ExceptionHandler(Exception.class)
    public ResponseEntity<ErrorMessage> serverErrorHandler(final Exception exception) {
        logger.error("Server error", exception);
        return new ResponseEntity<>(
                new ErrorMessage("Server Error"),
                HttpStatus.INTERNAL_SERVER_ERROR
        );
    }
}
