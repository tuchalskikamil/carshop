package pl.tuchalski.kamil.carshop.configuration;

import org.modelmapper.ModelMapper;
import org.modelmapper.TypeMap;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.reactive.function.client.WebClient;
import pl.tuchalski.kamil.carshop.car.Car;
import pl.tuchalski.kamil.carshop.car.CarIdToCarConverter;
import pl.tuchalski.kamil.carshop.car.CarSaveDto;
import pl.tuchalski.kamil.carshop.carDealer.CarDealerIdToCarDealerConverter;
import pl.tuchalski.kamil.carshop.carPurchase.CarPurchase;
import pl.tuchalski.kamil.carshop.carPurchase.CarPurchaseDto;
import pl.tuchalski.kamil.carshop.client.ClientIdToClientConverter;

/**
 * Configuration class
 */
@Configuration
public class CarShopConfiguration {

    /**
     * Instantiates configuration
     *
     * @param carIdToCarConverter       converter car id to car
     * @param clientIdToClientConverter converter client id to client
     * @return model mapper used in carPurchaseService
     */
    @Bean
    public ModelMapper modelMapper(final CarIdToCarConverter carIdToCarConverter,
                                   final ClientIdToClientConverter clientIdToClientConverter,
                                   final CarDealerIdToCarDealerConverter carDealerIdToCarDealerConverter) {
        final ModelMapper modelMapper = new ModelMapper();
        carPurchaseTypeMap(carIdToCarConverter, clientIdToClientConverter, carDealerIdToCarDealerConverter,
                modelMapper);
        carSaveTypeMap(modelMapper);
        return modelMapper;
    }

    @Bean
    public WebClient webClient() {
        return WebClient.create("http://localhost:3001");
    }


    private void carPurchaseTypeMap(final CarIdToCarConverter carIdToCarConverter,
                                    final ClientIdToClientConverter clientIdToClientConverter,
                                    final CarDealerIdToCarDealerConverter carDealerIdToCarDealerConverter,
                                    final ModelMapper modelMapper) {
        TypeMap<CarPurchaseDto, CarPurchase> propertyMapper =
                modelMapper.createTypeMap(CarPurchaseDto.class, CarPurchase.class);
        propertyMapper.addMappings(
                mapper -> {
                    mapper.using(carIdToCarConverter).map(CarPurchaseDto::getCarId, CarPurchase::setCar);
                    mapper.using(clientIdToClientConverter).map(CarPurchaseDto::getClientId, CarPurchase::setClient);
                    mapper.using(carDealerIdToCarDealerConverter).map(CarPurchaseDto::getCarDealerId,
                            CarPurchase::setUser);

                }
        );
    }

    private void carSaveTypeMap(final ModelMapper modelMapper) {
        TypeMap<CarSaveDto, Car> propertyMapper =
                modelMapper.createTypeMap(CarSaveDto.class, Car.class);
        propertyMapper.addMappings(
                mapper -> mapper.map(x -> false, Car::setSold)
        );
    }

}
